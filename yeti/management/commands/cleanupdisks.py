import logging

from django.conf import settings
from django.core.management.base import BaseCommand, CommandError

from yeti.tasks.maintenance import cleanup, clean_san

logger = logging.getLogger(__name__)

class Command(BaseCommand):
    args = '<number of days to cleanup>'
    help = 'Removes output media past the specified date'
        
    def handle(self, *args, **options):
        days = 7 if not len(args) else args[0]
        
        cleanup.delay(days)
        clean_san(days)