import ast
import logging

import cPickle as pickle

from celery import Celery

from librabbitmq import ConnectionError

from django.conf import settings
from django.core.management.base import NoArgsCommand, CommandError

from yeti.models import Asset, Task

logger = logging.getLogger(__name__)

def my_monitor(app):
    state = app.events.State()
    
    def handle(event):
        """
        
        """
        
        # print state.alive_workers()
        # # print state.event()
        # print state.event_callback
        # print state.event_count
        # print state.itertasks()
        # print state.task_types()
        # print state.tasks
        # print state.tasks_by_timestamp()
        # # print state.tasks_by_type()
        # # print state.tasks_by_worker()
        # # print state.worker_event()
        # print state.workers
        
        # the following are removed from the dictionary after the state
        # is updated so we must extract these before that happens
        
        kind = event.get('type')
        uuid = event.get('uuid')
        hostname = event.get('hostname')
        
        # upldate the in-memory state
        state.event(event)
        
        if kind == 'worker-heartbeat':
            processed = event.get('processed')
            active = event.get('active')
            
            known_workers = state.workers.keys()
            alive_workers = [worker['hostname'] for worker in state.alive_workers()]
            
            for hostname in known_workers:
                if hostname not in alive_workers:
                    print '%s went offline' % hostname
                    del state.workers[hostname]
            
            return
        elif kind == 'worker-online':
            logger.info('%s came online.', hostname)
            return
        elif kind == 'task-received':
            
            name = event.get('name')
            
            logger.debug('%s - %s | %s : %s', uuid, kind, name, hostname)
            
            if name in ('yeti.tasks.encode_video', 'yeti.tasks.ftp_upload', 'yeti.tasks.http_import', 'yeti.tasks.video_thumbs'):
                args = ast.literal_eval("%s" % event.get('args'))
                Task.objects.filter(pk=args[0]).update(amqp_id=uuid, hostname=hostname, status='received')
            
            return
        
        if kind == 'task-succeeded':
            logger.debug('%s - %s | completed in %f seconds', uuid, kind, event.get('runtime'))
        else:
            logger.debug('%s - %s', uuid, kind)
        
        if kind in ('task-succeeded', 'task-started'):
            task = Task.objects.filter(amqp_id=uuid)
            
            if kind == 'task-succeeded':
                task.update(status='succeeded')
            elif kind == 'task-started':
                task.update(status='started')
                
        elif kind == 'task-failed':
            print event.items()
            
            try:
                task = Task.objects.get(amqp_id=uuid)
            except Task.DoesNotExist:
                print 'not found', uuid
                return
            
            task.status = 'failed'
            
            def update_children(task):            
                for sub in Task.objects.filter(parent=task):
                    sub.status = 'failed'
                    sub.save()
                    
                    update_children(sub)
            
            update_children(task)
            
            exception = event.get('exception')
            traceback = event.get('traceback')
            
            task.save()
            
            Asset.objects.filter(id=task.asset.pk).update(status='failed')

    with app.connection() as connection:
        recv = app.events.Receiver(connection, handlers={'*': handle})
        
        try:
            recv.capture(limit=None, timeout=None, wakeup=True)
        except ConnectionError, e:
            print '\rExiting...'

class Command(NoArgsCommand):
    def handle_noargs(self, **options):
        
        logger.info('Connecting to broker "%s".', settings.BROKER_URL)
    
        celery = Celery(broker=settings.BROKER_URL)
        
        try:
            my_monitor(celery)
        except (KeyboardInterrupt, SystemExit):
            import sys
            sys.stderr.write('\r')