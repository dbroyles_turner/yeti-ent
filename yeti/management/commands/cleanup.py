import logging

from django.conf import settings
from django.core.management.base import NoArgsCommand, CommandError

from yeti.models import Asset

logger = logging.getLogger(__name__)

class Command(NoArgsCommand):
    def handle_noargs(self, **options):
        for asset in Asset.objects.all():
            status = set(asset.task_set.values_list('status', flat=True))
            
            
            if asset.status == 'failed':
                asset.task_set.filter(status='processing').update(status='failed')
            
            if len(status) == 1 and asset.status != status.pop():
                asset.status = 'completed'
                asset.save(update_fields=['status'])