from django.contrib import admin

from yeti.models import Asset, Property, Task, Output, M3U8

class AssetAdmin(admin.ModelAdmin):
    list_display = ('id', 'slug', 'status', 'get_progress', 'admin_property', 'type', 'created')
    fields = ('property', 'file', 'field_name', 'url', 'type', 'content_type', 'filename', 'size', 'tasks')
    readonly_fields = ('field_name', 'url', 'type', 'content_type', 'filename', 'size', 'tasks')
            
admin.site.register(Asset, AssetAdmin)
admin.site.register(Property)
admin.site.register(Task)
admin.site.register(Output)
admin.site.register(M3U8)
