from django.contrib import admin
from django.conf import settings
from django.conf.urls import patterns, include, url

admin.autodiscover()

urlpatterns = patterns('yeti.views.frontend',
    url(r'^$', 'main', name='main'),
    url(r'^asset/(\d+)/$', 'asset', name='asset'),
    url(r'^properties/$', 'properties', name='properties'),
    url(r'^users/$', 'users', name='users'),
)

urlpatterns += patterns('yeti.views.api',
    url(r'^api/asset/(\d+)/outputs/ftp/$', 'asset_ftp_outputs', name='asset_ftp_outputs'),
    url(r'^api/asset/(\d+)/outputs/final/$', 'asset_final_outputs', name='asset_final_outputs'),
    url(r'^api/asset/(\d+)/outputs/$', 'asset_outputs', name='asset_outputs'),
    url(r'^api/asset/(\d+)/task/([\w-]+)/$', 'asset_task_by_name', name='asset_task_by_name'),
    url(r'^api/asset/(\d+)/task/(\d+)/$', 'asset_task', name='asset_task'),
    url(r'^api/asset/(\d+)/tasks/$', 'asset_tasks', name='asset_tasks'),
    url(r'^api/asset/(\d+)/$', 'asset_details', name='asset_details'),
    url(r'^api/asset/$', 'asset', name='asset'),
)

urlpatterns += patterns('yeti.views.docs',
    url(r'^docs/$', 'main', name='docs'),
    url(r'^docs/steps/$', 'steps', name='docs_steps'),
    url(r'^docs/js/$', 'js', name='docs_js'),
)


urlpatterns += patterns('',
    url(r'^admin/', include(admin.site.urls)),
)

if settings.DEBUG:
    from django.conf.urls.static import static
    
    urlpatterns += static(settings.MEDIA_URL, document_root=settings.MEDIA_ROOT)
    
    # r'^media/(?P<path>.*)$
    
    urlpatterns += patterns('',
        url(r'^(?P<path>.*)$', 'django.views.static.serve', {
            'document_root': settings.STATIC_ROOT,
        }),
   )