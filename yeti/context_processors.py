def stomp_url(request):
    from django.conf import settings
    
    return {'STOMP_SERVER': settings.STOMP_SERVER}