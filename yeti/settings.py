from os.path import abspath, dirname, join

PROJECT_PATH = dirname(dirname(abspath(__file__)))

DEBUG = True
TEMPLATE_DEBUG = DEBUG

ALLOWED_HOSTS = ['yeti.turner.com']

ADMINS = (
    ('Hunter Ford', 'hunter.ford@turner.com'),
)

MANAGERS = ADMINS

DATABASES = {
    'default': {
        'ENGINE': 'django.db.backends.mysql',
        'NAME': 'yeti_prd',
        'HOST': 'yeti8mysql1.turner.com',
        'USER': 'yeti_user',
        'PASSWORD': 'y3t1pr0dd',
    },
    'secondary': {
        'ENGINE': 'django.db.backends.mysql',
        'NAME': 'yeti_prd',
        'HOST': 'yeti8mysql2.turner.com',
        'USER': 'yeti_user',
        'PASSWORD': 'y3t1pr0dd',
    }
}

DATABASE_ROUTERS = ['yeti.routers.DBRouter']

TIME_ZONE = 'America/New_York'
LANGUAGE_CODE = 'en-us'

SITE_ID = 1

USE_I18N = False
USE_L10N = True
USE_TZ = True

MEDIA_ROOT = join(PROJECT_PATH, 'media')
MEDIA_URL = '/media/'

STATIC_ROOT = join(PROJECT_PATH, 'static')
STATIC_URL = '/static/'

STATICFILES_FINDERS = (
    'django.contrib.staticfiles.finders.FileSystemFinder',
    'django.contrib.staticfiles.finders.AppDirectoriesFinder',
)

SECRET_KEY = 'vvvwivisoc&yi6#zi2q&)6@*6!c-q@tvoey!*1c-3#2$oo!#li'

TEMPLATE_LOADERS = (
    ('django.template.loaders.cached.Loader', (
        'django.template.loaders.filesystem.Loader',
        'django.template.loaders.app_directories.Loader',
    )),
)

MIDDLEWARE_CLASSES = (
    'django.middleware.common.CommonMiddleware',
    'django.contrib.sessions.middleware.SessionMiddleware',
    'django.middleware.csrf.CsrfViewMiddleware',
    'django.contrib.auth.middleware.AuthenticationMiddleware',
    'django.contrib.messages.middleware.MessageMiddleware',
    'django.middleware.transaction.TransactionMiddleware',
)

ROOT_URLCONF = 'yeti.urls'

WSGI_APPLICATION = 'yeti.wsgi.application'

INSTALLED_APPS = (
    'django.contrib.auth',
    'django.contrib.contenttypes',
    'django.contrib.sessions',
    'django.contrib.sites',
    'django.contrib.messages',
    'django.contrib.staticfiles',
    'django.contrib.admin',
    'django.contrib.humanize',
    'yeti',
    'djcelery',
)

TEMPLATE_CONTEXT_PROCESSORS = (
    'django.contrib.auth.context_processors.auth',
    'django.core.context_processors.debug',
    'django.core.context_processors.i18n',
    'django.core.context_processors.media',
    'django.core.context_processors.static',
    'django.core.context_processors.tz',
    'django.contrib.messages.context_processors.messages',
    'yeti.context_processors.stomp_url'
)

LOGGING = {
    'version': 1,
    'disable_existing_loggers': False,
    'filters': {
        'require_debug_false': {
            '()': 'django.utils.log.RequireDebugFalse'
        }
    },
    'formatters': {
        'verbose': {
            'format': '%(asctime)s - %(name)s - %(funcName)s [%(levelname)s] %(message)s'
        },
        'simple': {
            'format': '%(levelname)s: %(message)s'
        },
    },
    'handlers': {
        'null': {
            'level':'DEBUG',
            'class':'django.utils.log.NullHandler',
        },
        'console':{
            'level':'DEBUG',
            'class':'logging.StreamHandler',
            'formatter': 'verbose',
        },
        'mail_admins': {
            'level': 'ERROR',
            'class': 'django.utils.log.AdminEmailHandler',
            'filters': ['require_debug_false'],
        }
    },
    'loggers': {
        'django': {
            'handlers':['console'],
            'level':'WARNING',
            'propagate': True,
        },
        'django.request': {
            'handlers': ['mail_admins'],
            'level': 'ERROR',
            'propagate': False,
        },
        'celery': {
            'handlers': ['console'],
            'level': 'WARNING',
            'propagate': True,
        },
        'yeti': {
            'handlers': ['console'],
            'level': 'WARNING',
            'propagate': True,
        }
    }
}

CACHES = {
    'default': {
        'BACKEND': 'django.core.cache.backends.memcached.PyLibMCCache',
        'LOCATION': [
            'ff10web1:11211',
            'ff10web2:11211',
            'ff10web3:11211',
            'ff10web4:11211',
        ]
    }
}

SESSION_ENGINE = 'django.contrib.sessions.backends.cache' # cached_db

BROKER_URL = 'amqp://yeti:doc0frayed@ff10mgr1:5672/yeti'
PROGRESS_BROKER_URL = 'amqp://guest:guest@ff10mgr1:5672//'
STOMP_SERVER = 'http://ff10mgr1:15674/stomp'

# CELERY_RESULT_DBURI = 'mysql://root:@localhost/celery'

from kombu.common import Broadcast, Exchange, Queue

CELERY_QUEUES = (
    Queue('default', Exchange('default'), routing_key='default'),
    Broadcast('maintenance'),
)

CELERY_ACKS_LATE = True
CELERY_CACHE_BACKEND = 'memcached://ff10web1:11211;ff10web2:11211;ff10web3:11211;ff10web4:11211/'
CELERY_DEFAULT_QUEUE = 'default'
CELERY_DISABLE_RATE_LIMITS = True
CELERYD_HIJACK_ROOT_LOGGER = False
CELERY_IGNORE_RESULT = True
CELERY_IMPORTS = ('yeti.tasks.video', 'yeti.tasks.db', 'yeti.tasks.ftp', 'yeti.tasks.maintenance')
CELERYD_PREFETCH_MULTIPLIER = 1
CELERY_RESULT_BACKEND = 'amqp'
CELERY_ROUTES = {'yeti.tasks.maintenance.cleanup': {'queue': 'maintenance'}}
CELERY_SEND_EVENTS = True
CELERY_SEND_TASK_SENT_EVENT = False
CELERYD_STATE_DB = join(PROJECT_PATH, 'worker-state')
CELERY_TASK_RESULT_EXPIRES = 60 * 60 * 5
CELERY_TRACK_STARTED = True
CELERY_WORKER_DIRECT = True

CELERYBEAT_SCHEDULER = 'djcelery.schedulers.DatabaseScheduler'

SAN_PATH = '/video/'
SAN_WEB = 'http://any10http1/'

LOCAL_WEB_SERVER = 'http://%s'

try:
    from development import *
except ImportError:
    pass

import djcelery

from celery.signals import setup_logging

@setup_logging.connect
def task_sent_handler(signal=None, sender=None, loglevel=None, logfile=None, format=None, colorize=None):
    pass

djcelery.setup_loader()