# -*- coding: utf-8 -*-
import datetime
from south.db import db
from south.v2 import SchemaMigration
from django.db import models


class Migration(SchemaMigration):

    def forwards(self, orm):
        # Adding model 'Property'
        db.create_table(u'yeti_property', (
            (u'id', self.gf('django.db.models.fields.AutoField')(primary_key=True)),
            ('name', self.gf('django.db.models.fields.CharField')(max_length=75)),
            ('slug', self.gf('django.db.models.fields.CharField')(max_length=100)),
            ('storage', self.gf('django.db.models.fields.CharField')(max_length=200)),
        ))
        db.send_create_signal(u'yeti', ['Property'])

        # Adding model 'Worker'
        db.create_table(u'yeti_worker', (
            (u'id', self.gf('django.db.models.fields.AutoField')(primary_key=True)),
            ('hostname', self.gf('django.db.models.fields.CharField')(unique=True, max_length=100)),
        ))
        db.send_create_signal(u'yeti', ['Worker'])

        # Adding model 'Asset'
        db.create_table(u'yeti_asset', (
            (u'id', self.gf('django.db.models.fields.AutoField')(primary_key=True)),
            ('property', self.gf('django.db.models.fields.related.ForeignKey')(to=orm['yeti.Property'])),
            ('file', self.gf('django.db.models.fields.CharField')(max_length=255)),
            ('type', self.gf('django.db.models.fields.PositiveSmallIntegerField')(default=0)),
            ('content_type', self.gf('django.db.models.fields.CharField')(max_length=50, blank=True)),
            ('filename', self.gf('django.db.models.fields.CharField')(max_length=100, blank=True)),
            ('basename', self.gf('django.db.models.fields.CharField')(max_length=100, blank=True)),
            ('extension', self.gf('django.db.models.fields.CharField')(max_length=10, blank=True)),
            ('size', self.gf('django.db.models.fields.PositiveIntegerField')(null=True, blank=True)),
            ('created', self.gf('django.db.models.fields.DateTimeField')(auto_now_add=True, blank=True)),
            ('width', self.gf('django.db.models.fields.PositiveSmallIntegerField')(null=True, blank=True)),
            ('height', self.gf('django.db.models.fields.PositiveSmallIntegerField')(null=True, blank=True)),
            ('duration', self.gf('django.db.models.fields.PositiveIntegerField')(null=True, blank=True)),
            ('bit_rate', self.gf('django.db.models.fields.PositiveIntegerField')(null=True, blank=True)),
            ('container_format', self.gf('django.db.models.fields.CharField')(max_length=100, blank=True)),
            ('sar', self.gf('django.db.models.fields.CharField')(max_length=100, blank=True)),
            ('dar', self.gf('django.db.models.fields.CharField')(max_length=100, blank=True)),
            ('video_codec', self.gf('django.db.models.fields.CharField')(max_length=100, blank=True)),
            ('video_bit_rate', self.gf('django.db.models.fields.PositiveIntegerField')(null=True, blank=True)),
            ('video_frame_rate', self.gf('django.db.models.fields.CharField')(max_length=100, blank=True)),
            ('audio_codec', self.gf('django.db.models.fields.CharField')(max_length=100, blank=True)),
            ('audio_bit_rate', self.gf('django.db.models.fields.PositiveIntegerField')(null=True, blank=True)),
            ('audio_channels', self.gf('django.db.models.fields.PositiveSmallIntegerField')(null=True, blank=True)),
            ('audio_sample_rate', self.gf('django.db.models.fields.PositiveIntegerField')(null=True, blank=True)),
            ('probe_output', self.gf('django.db.models.fields.TextField')(blank=True)),
            ('title', self.gf('django.db.models.fields.CharField')(max_length=150, blank=True)),
            ('slug', self.gf('django.db.models.fields.CharField')(max_length=200, blank=True)),
            ('field_name', self.gf('django.db.models.fields.CharField')(max_length=100)),
            ('url', self.gf('django.db.models.fields.URLField')(max_length=200, blank=True)),
            ('progress', self.gf('django.db.models.fields.PositiveSmallIntegerField')(default=0)),
            ('status', self.gf('django.db.models.fields.CharField')(max_length=100, blank=True)),
            ('is_test', self.gf('django.db.models.fields.BooleanField')(default=False)),
        ))
        db.send_create_signal(u'yeti', ['Asset'])

        # Adding model 'Task'
        db.create_table(u'yeti_task', (
            (u'id', self.gf('django.db.models.fields.AutoField')(primary_key=True)),
            ('parent', self.gf('django.db.models.fields.related.ForeignKey')(to=orm['yeti.Task'], null=True, blank=True)),
            ('asset', self.gf('django.db.models.fields.related.ForeignKey')(to=orm['yeti.Asset'])),
            ('name', self.gf('django.db.models.fields.CharField')(max_length=100)),
            ('process', self.gf('django.db.models.fields.CharField')(max_length=100)),
            ('parameters', self.gf('django.db.models.fields.TextField')()),
            ('progress', self.gf('django.db.models.fields.PositiveSmallIntegerField')(default=0)),
            ('speed', self.gf('django.db.models.fields.DecimalField')(null=True, max_digits=5, decimal_places=2, blank=True)),
            ('hostname', self.gf('django.db.models.fields.CharField')(max_length=100)),
            ('error_message', self.gf('django.db.models.fields.TextField')(blank=True)),
            ('status', self.gf('django.db.models.fields.CharField')(default='created', max_length=100)),
            ('amqp_id', self.gf('django.db.models.fields.CharField')(max_length=100)),
        ))
        db.send_create_signal(u'yeti', ['Task'])

        # Adding model 'Output'
        db.create_table(u'yeti_output', (
            (u'id', self.gf('django.db.models.fields.AutoField')(primary_key=True)),
            ('property', self.gf('django.db.models.fields.related.ForeignKey')(to=orm['yeti.Property'])),
            ('file', self.gf('django.db.models.fields.CharField')(max_length=255)),
            ('type', self.gf('django.db.models.fields.PositiveSmallIntegerField')(default=0)),
            ('content_type', self.gf('django.db.models.fields.CharField')(max_length=50, blank=True)),
            ('filename', self.gf('django.db.models.fields.CharField')(max_length=100, blank=True)),
            ('basename', self.gf('django.db.models.fields.CharField')(max_length=100, blank=True)),
            ('extension', self.gf('django.db.models.fields.CharField')(max_length=10, blank=True)),
            ('size', self.gf('django.db.models.fields.PositiveIntegerField')(null=True, blank=True)),
            ('created', self.gf('django.db.models.fields.DateTimeField')(auto_now_add=True, blank=True)),
            ('width', self.gf('django.db.models.fields.PositiveSmallIntegerField')(null=True, blank=True)),
            ('height', self.gf('django.db.models.fields.PositiveSmallIntegerField')(null=True, blank=True)),
            ('duration', self.gf('django.db.models.fields.PositiveIntegerField')(null=True, blank=True)),
            ('bit_rate', self.gf('django.db.models.fields.PositiveIntegerField')(null=True, blank=True)),
            ('container_format', self.gf('django.db.models.fields.CharField')(max_length=100, blank=True)),
            ('sar', self.gf('django.db.models.fields.CharField')(max_length=100, blank=True)),
            ('dar', self.gf('django.db.models.fields.CharField')(max_length=100, blank=True)),
            ('video_codec', self.gf('django.db.models.fields.CharField')(max_length=100, blank=True)),
            ('video_bit_rate', self.gf('django.db.models.fields.PositiveIntegerField')(null=True, blank=True)),
            ('video_frame_rate', self.gf('django.db.models.fields.CharField')(max_length=100, blank=True)),
            ('audio_codec', self.gf('django.db.models.fields.CharField')(max_length=100, blank=True)),
            ('audio_bit_rate', self.gf('django.db.models.fields.PositiveIntegerField')(null=True, blank=True)),
            ('audio_channels', self.gf('django.db.models.fields.PositiveSmallIntegerField')(null=True, blank=True)),
            ('audio_sample_rate', self.gf('django.db.models.fields.PositiveIntegerField')(null=True, blank=True)),
            ('probe_output', self.gf('django.db.models.fields.TextField')(blank=True)),
            ('asset', self.gf('django.db.models.fields.related.ForeignKey')(to=orm['yeti.Asset'])),
            ('task', self.gf('django.db.models.fields.related.ForeignKey')(to=orm['yeti.Task'])),
            ('hostname', self.gf('django.db.models.fields.CharField')(max_length=50, blank=True)),
            ('ssim', self.gf('django.db.models.fields.DecimalField')(null=True, max_digits=8, decimal_places=7, blank=True)),
            ('psnr', self.gf('django.db.models.fields.DecimalField')(null=True, max_digits=5, decimal_places=3, blank=True)),
            ('rate_factor', self.gf('django.db.models.fields.DecimalField')(null=True, max_digits=5, decimal_places=2, blank=True)),
        ))
        db.send_create_signal(u'yeti', ['Output'])

        # Adding model 'M3U8'
        db.create_table(u'yeti_m3u8', (
            (u'id', self.gf('django.db.models.fields.AutoField')(primary_key=True)),
            ('asset', self.gf('django.db.models.fields.related.ForeignKey')(to=orm['yeti.Asset'])),
            ('task', self.gf('django.db.models.fields.related.ForeignKey')(to=orm['yeti.Task'])),
            ('folder', self.gf('django.db.models.fields.CharField')(max_length=200)),
            ('playlist', self.gf('django.db.models.fields.CharField')(max_length=100)),
            ('created', self.gf('django.db.models.fields.DateTimeField')(auto_now_add=True, blank=True)),
        ))
        db.send_create_signal(u'yeti', ['M3U8'])


    def backwards(self, orm):
        # Deleting model 'Property'
        db.delete_table(u'yeti_property')

        # Deleting model 'Worker'
        db.delete_table(u'yeti_worker')

        # Deleting model 'Asset'
        db.delete_table(u'yeti_asset')

        # Deleting model 'Task'
        db.delete_table(u'yeti_task')

        # Deleting model 'Output'
        db.delete_table(u'yeti_output')

        # Deleting model 'M3U8'
        db.delete_table(u'yeti_m3u8')


    models = {
        u'yeti.asset': {
            'Meta': {'object_name': 'Asset'},
            'audio_bit_rate': ('django.db.models.fields.PositiveIntegerField', [], {'null': 'True', 'blank': 'True'}),
            'audio_channels': ('django.db.models.fields.PositiveSmallIntegerField', [], {'null': 'True', 'blank': 'True'}),
            'audio_codec': ('django.db.models.fields.CharField', [], {'max_length': '100', 'blank': 'True'}),
            'audio_sample_rate': ('django.db.models.fields.PositiveIntegerField', [], {'null': 'True', 'blank': 'True'}),
            'basename': ('django.db.models.fields.CharField', [], {'max_length': '100', 'blank': 'True'}),
            'bit_rate': ('django.db.models.fields.PositiveIntegerField', [], {'null': 'True', 'blank': 'True'}),
            'container_format': ('django.db.models.fields.CharField', [], {'max_length': '100', 'blank': 'True'}),
            'content_type': ('django.db.models.fields.CharField', [], {'max_length': '50', 'blank': 'True'}),
            'created': ('django.db.models.fields.DateTimeField', [], {'auto_now_add': 'True', 'blank': 'True'}),
            'dar': ('django.db.models.fields.CharField', [], {'max_length': '100', 'blank': 'True'}),
            'duration': ('django.db.models.fields.PositiveIntegerField', [], {'null': 'True', 'blank': 'True'}),
            'extension': ('django.db.models.fields.CharField', [], {'max_length': '10', 'blank': 'True'}),
            'field_name': ('django.db.models.fields.CharField', [], {'max_length': '100'}),
            'file': ('django.db.models.fields.CharField', [], {'max_length': '255'}),
            'filename': ('django.db.models.fields.CharField', [], {'max_length': '100', 'blank': 'True'}),
            'height': ('django.db.models.fields.PositiveSmallIntegerField', [], {'null': 'True', 'blank': 'True'}),
            u'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'is_test': ('django.db.models.fields.BooleanField', [], {'default': 'False'}),
            'probe_output': ('django.db.models.fields.TextField', [], {'blank': 'True'}),
            'progress': ('django.db.models.fields.PositiveSmallIntegerField', [], {'default': '0'}),
            'property': ('django.db.models.fields.related.ForeignKey', [], {'to': u"orm['yeti.Property']"}),
            'sar': ('django.db.models.fields.CharField', [], {'max_length': '100', 'blank': 'True'}),
            'size': ('django.db.models.fields.PositiveIntegerField', [], {'null': 'True', 'blank': 'True'}),
            'slug': ('django.db.models.fields.CharField', [], {'max_length': '200', 'blank': 'True'}),
            'status': ('django.db.models.fields.CharField', [], {'max_length': '100', 'blank': 'True'}),
            'title': ('django.db.models.fields.CharField', [], {'max_length': '150', 'blank': 'True'}),
            'type': ('django.db.models.fields.PositiveSmallIntegerField', [], {'default': '0'}),
            'url': ('django.db.models.fields.URLField', [], {'max_length': '200', 'blank': 'True'}),
            'video_bit_rate': ('django.db.models.fields.PositiveIntegerField', [], {'null': 'True', 'blank': 'True'}),
            'video_codec': ('django.db.models.fields.CharField', [], {'max_length': '100', 'blank': 'True'}),
            'video_frame_rate': ('django.db.models.fields.CharField', [], {'max_length': '100', 'blank': 'True'}),
            'width': ('django.db.models.fields.PositiveSmallIntegerField', [], {'null': 'True', 'blank': 'True'})
        },
        u'yeti.m3u8': {
            'Meta': {'object_name': 'M3U8'},
            'asset': ('django.db.models.fields.related.ForeignKey', [], {'to': u"orm['yeti.Asset']"}),
            'created': ('django.db.models.fields.DateTimeField', [], {'auto_now_add': 'True', 'blank': 'True'}),
            'folder': ('django.db.models.fields.CharField', [], {'max_length': '200'}),
            u'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'playlist': ('django.db.models.fields.CharField', [], {'max_length': '100'}),
            'task': ('django.db.models.fields.related.ForeignKey', [], {'to': u"orm['yeti.Task']"})
        },
        u'yeti.output': {
            'Meta': {'object_name': 'Output'},
            'asset': ('django.db.models.fields.related.ForeignKey', [], {'to': u"orm['yeti.Asset']"}),
            'audio_bit_rate': ('django.db.models.fields.PositiveIntegerField', [], {'null': 'True', 'blank': 'True'}),
            'audio_channels': ('django.db.models.fields.PositiveSmallIntegerField', [], {'null': 'True', 'blank': 'True'}),
            'audio_codec': ('django.db.models.fields.CharField', [], {'max_length': '100', 'blank': 'True'}),
            'audio_sample_rate': ('django.db.models.fields.PositiveIntegerField', [], {'null': 'True', 'blank': 'True'}),
            'basename': ('django.db.models.fields.CharField', [], {'max_length': '100', 'blank': 'True'}),
            'bit_rate': ('django.db.models.fields.PositiveIntegerField', [], {'null': 'True', 'blank': 'True'}),
            'container_format': ('django.db.models.fields.CharField', [], {'max_length': '100', 'blank': 'True'}),
            'content_type': ('django.db.models.fields.CharField', [], {'max_length': '50', 'blank': 'True'}),
            'created': ('django.db.models.fields.DateTimeField', [], {'auto_now_add': 'True', 'blank': 'True'}),
            'dar': ('django.db.models.fields.CharField', [], {'max_length': '100', 'blank': 'True'}),
            'duration': ('django.db.models.fields.PositiveIntegerField', [], {'null': 'True', 'blank': 'True'}),
            'extension': ('django.db.models.fields.CharField', [], {'max_length': '10', 'blank': 'True'}),
            'file': ('django.db.models.fields.CharField', [], {'max_length': '255'}),
            'filename': ('django.db.models.fields.CharField', [], {'max_length': '100', 'blank': 'True'}),
            'height': ('django.db.models.fields.PositiveSmallIntegerField', [], {'null': 'True', 'blank': 'True'}),
            'hostname': ('django.db.models.fields.CharField', [], {'max_length': '50', 'blank': 'True'}),
            u'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'probe_output': ('django.db.models.fields.TextField', [], {'blank': 'True'}),
            'property': ('django.db.models.fields.related.ForeignKey', [], {'to': u"orm['yeti.Property']"}),
            'psnr': ('django.db.models.fields.DecimalField', [], {'null': 'True', 'max_digits': '5', 'decimal_places': '3', 'blank': 'True'}),
            'rate_factor': ('django.db.models.fields.DecimalField', [], {'null': 'True', 'max_digits': '5', 'decimal_places': '2', 'blank': 'True'}),
            'sar': ('django.db.models.fields.CharField', [], {'max_length': '100', 'blank': 'True'}),
            'size': ('django.db.models.fields.PositiveIntegerField', [], {'null': 'True', 'blank': 'True'}),
            'ssim': ('django.db.models.fields.DecimalField', [], {'null': 'True', 'max_digits': '8', 'decimal_places': '7', 'blank': 'True'}),
            'task': ('django.db.models.fields.related.ForeignKey', [], {'to': u"orm['yeti.Task']"}),
            'type': ('django.db.models.fields.PositiveSmallIntegerField', [], {'default': '0'}),
            'video_bit_rate': ('django.db.models.fields.PositiveIntegerField', [], {'null': 'True', 'blank': 'True'}),
            'video_codec': ('django.db.models.fields.CharField', [], {'max_length': '100', 'blank': 'True'}),
            'video_frame_rate': ('django.db.models.fields.CharField', [], {'max_length': '100', 'blank': 'True'}),
            'width': ('django.db.models.fields.PositiveSmallIntegerField', [], {'null': 'True', 'blank': 'True'})
        },
        u'yeti.property': {
            'Meta': {'object_name': 'Property'},
            u'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'name': ('django.db.models.fields.CharField', [], {'max_length': '75'}),
            'slug': ('django.db.models.fields.CharField', [], {'max_length': '100'}),
            'storage': ('django.db.models.fields.CharField', [], {'max_length': '200'})
        },
        u'yeti.task': {
            'Meta': {'object_name': 'Task'},
            'amqp_id': ('django.db.models.fields.CharField', [], {'max_length': '100'}),
            'asset': ('django.db.models.fields.related.ForeignKey', [], {'to': u"orm['yeti.Asset']"}),
            'error_message': ('django.db.models.fields.TextField', [], {'blank': 'True'}),
            'hostname': ('django.db.models.fields.CharField', [], {'max_length': '100'}),
            u'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'name': ('django.db.models.fields.CharField', [], {'max_length': '100'}),
            'parameters': ('django.db.models.fields.TextField', [], {}),
            'parent': ('django.db.models.fields.related.ForeignKey', [], {'to': u"orm['yeti.Task']", 'null': 'True', 'blank': 'True'}),
            'process': ('django.db.models.fields.CharField', [], {'max_length': '100'}),
            'progress': ('django.db.models.fields.PositiveSmallIntegerField', [], {'default': '0'}),
            'speed': ('django.db.models.fields.DecimalField', [], {'null': 'True', 'max_digits': '5', 'decimal_places': '2', 'blank': 'True'}),
            'status': ('django.db.models.fields.CharField', [], {'default': "'created'", 'max_length': '100'})
        },
        u'yeti.worker': {
            'Meta': {'object_name': 'Worker'},
            'hostname': ('django.db.models.fields.CharField', [], {'unique': 'True', 'max_length': '100'}),
            u'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'})
        }
    }

    complete_apps = ['yeti']