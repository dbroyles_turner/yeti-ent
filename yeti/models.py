import time
import logging
import os.path

from datetime import datetime
from subprocess import Popen, PIPE

from django.db import models
from django.conf import settings
from django.db.models import Sum
from django.utils import timezone
from django.forms.models import model_to_dict
from django.contrib.contenttypes import generic
from django.contrib.contenttypes.models import ContentType

from yeti.managers import TaskManager, OutputManager

logger = logging.getLogger(__name__)

MEDIA_TYPES = (
    (0, '----'),
    (1, 'Video'),
    (2, 'Image'),
    (3, 'Audio'),
    (4, 'PDF'),
)

class Property(models.Model):
    """
    Property to contain the asset
    """
    name = models.CharField(max_length=75)
    slug = models.CharField(max_length=100)
    storage = models.CharField(max_length=200)
    
    class Meta:
        verbose_name_plural = 'properties'
    
    def __unicode__(self):
        return self.name

class Worker(models.Model):
    hostname = models.CharField(max_length=100, unique=True)

def asset_upload_path(instance, filename):
    """
    Upload path for Asset source
    """
    
    # This ensures the filename is also saved with the Job
    # since Django will rename the file if it exists twice.
    instance.filename = filename
    
    root, ext = os.path.splitext(filename)
    instance.basename = root
    
    logger.debug('Got basename "%s" from filename "%s".', root, filename)
    
    path = os.path.join('src', timezone.now().strftime('%Y/%m/%d'), filename)
    
    logger.debug('Upload path "%s"', path)
    
    return path

class Media(models.Model):
    property = models.ForeignKey(Property, db_index=True)
    
    file = models.CharField(max_length=255)
    
    type = models.PositiveSmallIntegerField(choices=MEDIA_TYPES, default=0)
    
    content_type = models.CharField(max_length=50, blank=True)
    filename = models.CharField(max_length=100, blank=True)
    basename = models.CharField(max_length=100, blank=True)
    extension = models.CharField(max_length=10, blank=True)
    size = models.PositiveIntegerField(blank=True, null=True)
    
    created = models.DateTimeField(auto_now_add=True)
    
    width = models.PositiveSmallIntegerField(null=True, blank=True)
    height = models.PositiveSmallIntegerField(null=True, blank=True)
    
    duration = models.PositiveIntegerField(null=True, blank=True, help_text='in milliseconds')
    bit_rate = models.PositiveIntegerField(null=True, blank=True)
    container_format = models.CharField(max_length=100, blank=True)
    
    sar = models.CharField(max_length=100, blank=True)
    dar = models.CharField(max_length=100, blank=True)
    
    video_codec = models.CharField(max_length=100, blank=True)
    video_bit_rate = models.PositiveIntegerField(null=True, blank=True)
    video_frame_rate = models.CharField(max_length=100, blank=True)
    
    audio_codec = models.CharField(max_length=100, blank=True)
    audio_bit_rate = models.PositiveIntegerField(null=True, blank=True)
    audio_channels = models.PositiveSmallIntegerField(null=True, blank=True)
    audio_sample_rate = models.PositiveIntegerField(null=True, blank=True)
    
    probe_output = models.TextField(blank=True)
    
    class Meta:
        abstract = True

class Scenario(models.Model):
    name = models.CharField(max_length=100)
    property = models.ForeignKey(Property)
    steps = models.TextField()
    
    date_added = models.DateField(auto_now_add=True)
    date_modified = models.DateField(auto_now=True)
    
    class Meta:
        unique_together = ('name', 'property')

class Asset(Media):
    title = models.CharField(max_length=150, blank=True)
    slug = models.CharField(max_length=200, blank=True)
    
    field_name = models.CharField(max_length=100)
    
    url = models.URLField(blank=True)
    
    scenario = models.ForeignKey(Scenario, blank=True, null=True)
    
    progress = models.PositiveSmallIntegerField(default=0)
    status = models.CharField(max_length=100, blank=True)
    
    is_test = models.BooleanField(default=False)
    
    @models.permalink
    def get_absolute_url(self):
        return ('yeti.views.frontend.asset', [str(self.id)])
    
    def get_working_path(self):
        """
        If the file exists on the transcoder, return the media path.
        Otherwise, we are assuming it's an absolute path.
        """
        return self.file
    
    def get_input_location(self):
        """
        Determine the location of the input media. URL or file based.
        """
        if self.url:
            return self.url
        else:
            return self.file
    
    def get_progress(self):
        if self.status in ('failed','stopped','completed'):
            return 100
        elif self.status in ('created', 'started'):
            return 0
        else:        
            return self.progress
    
    def check_completion(self):
        states = set(Task.objects.filter(asset=self).values_list('status', flat=True))
        
        if len(states) == 1 and 'completed' in states:
            self.status = 'completed'
        elif 'failed' in states:
            self.status = 'failed'
        
        self.save(update_fields=['status'])
    
    def tasks(self):
        for task in Task.objects.filter(asset=self, parent=None):
            ttype = getattr(task, task.content_type.model)
    
    def serialize(self):
        fields = ('id', 'title', 'slug', 'content_type',
                  'filename', 'basename', 'size', 'created')
        obj = {}
        for field in fields:
            value = getattr(self, field)
            if isinstance(value, datetime):
                value = value.isoformat()
            obj[field] = value
        obj['type'] = self.get_type_display()
        return obj
    
    def admin_property(self):
        """
        Return the property name
        """
        
        return self.property.name
    
    admin_property.short_description = 'property'
    admin_property.admin_order_field = 'property__name'
    
    def progress_class(self):
        if self.status in ('failed',):
            return 'progress progress-danger'
        elif self.status in ('dependent', 'stopped', 'retrying'):
            return 'progress progress-warning'
        elif self.progress == 100 or self.status in ('completed',):
            return 'progress progress-success'
        else:
            return 'progress progress-striped active'
    
    def full_path(self):
        """
        A convenience method to return the path of the file, which
        would depend on whether it's saved as a path or a file.
        """
        return self.path if self.path else self.file.path
    
    def total_progress(self):
        count = self.task_set.count()
        total = self.task_set.aggregate(total=Sum('progress'))['total'] or 0
        
        try:
            percentage = int(float(total) / count)
        except ZeroDivisionError:
            percentage = 0
        
        if self.progress != percentage:
            self.progress = percentage
            self.save(update_fields=['progress'])
        
        logger.debug('Total progress: "%d%%"', percentage)
        
        return percentage

class Task(models.Model):
    parent = models.ForeignKey('self', blank=True, null=True)
    asset = models.ForeignKey(Asset)
    name = models.CharField(max_length=100)
    process = models.CharField(max_length=100)
    parameters = models.TextField()
    progress = models.PositiveSmallIntegerField(default=0)
    speed = models.DecimalField(max_digits=5, decimal_places=2, null=True, blank=True)
    
    hostname = models.CharField(max_length=100)
    
    start_date = models.DateTimeField(null=True, blank=True)
    end_date = models.DateTimeField(null=True, blank=True)
    
    time = models.PositiveIntegerField(null=True, blank=True)
    utime = models.PositiveIntegerField(null=True, blank=True)
    
    error_message = models.TextField(blank=True)
    
    status = models.CharField(max_length=100, default='created')
    amqp_id = models.CharField(max_length=100)
    
    objects = TaskManager()
    
    def save(self, *args, **kwargs):
        # progress can not be greater than 100
        self.progress = min(self.progress, 100)
        super(Task, self).save(*args, **kwargs)
    
    def fail(self, message='', status='failed'):
        self.asset.status = status
        self.asset.save(update_fields=['status'])
        
        def fail_task(task):
            task.status = status
            task.progress = 100
            task.error_message = message.rstrip('.')
            task.save(update_fields=['status', 'progress', 'error_message'])
            
            for child in Task.objects.filter(parent=task):
                fail_task(child)
        
        fail_task(self)
    
    def complete(self, time=None, utime=None):
        update_fields = ['progress', 'status', 'end_date']
        
        self.progress = 100
        self.status = 'completed'
        self.end_date = timezone.now()
        
        if time:
            self.time = time
            update_fields.append('time')
        
        if utime:
            self.utime = utime
            update_fields.append('utime')
        
        self.save(update_fields=update_fields)
    
    def get_display_class(self):
        if self.status in ('failed',):
            return 'danger'
        elif self.status in ('dependent', 'stopped', 'waiting', 'retrying'):
            return 'warning'
        elif self.status in ('completed',):
            return 'success'
        else:
            return 'striped active'
    
    def get_progress(self):
        if self.status in ('failed','stopped','completed'):
            return 100
        elif self.status in ('created', 'started'):
            return 0
        else:        
            return self.progress
    
    def progress_class(self):
        if self.status in ('failed',):
            return 'progress progress-danger'
        elif self.status in ('dependent', 'stopped', 'retrying'):
            return 'progress progress-warning'
        elif self.progress == 100 or self.status in ('completed',):
            return 'progress progress-success'
        else:
            return 'progress progress-striped active'
    
    def sanitized_params(self):
        """
        Return the parameters, stripping sensitive data like passwords.
        """
        import json
        
        data = json.loads(self.parameters)
        
        if 'password' in data:
            data['password'] = '********'
        
        return json.dumps(data, sort_keys=True, indent=2)

class Output(Media):
    asset = models.ForeignKey(Asset)
    task = models.ForeignKey(Task)
    
    hostname = models.CharField(max_length=50, blank=True)
    
    ssim = models.DecimalField(max_digits=8, decimal_places=7, null=True, blank=True)
    psnr = models.DecimalField(max_digits=5, decimal_places=3, null=True, blank=True)
    rate_factor = models.DecimalField(max_digits=5, decimal_places=2, null=True, blank=True)
    
    objects = OutputManager()
    
    def get_working_path(self):
        """
        If the file exists on the transcoder, return the media path.
        Otherwise, we are assuming it's an absolute path.
        """
        if self.hostname:
            from django.conf import settings
            from os.path import join
            return join(settings.MEDIA_ROOT, self.file)
        else:
            return self.file
    
    def local_url(self):
        """
        Get a URL usable by a client.
        """
        from django.conf import settings
        
        if self.hostname:
            from os.path import join

            relative_path = join(settings.MEDIA_URL, self.file)
            
            try:
                local_server = settings.LOCAL_WEB_SERVER % self.hostname
            except TypeError:
                local_server = settings.LOCAL_WEB_SERVER
            
            return '%s%s' % (local_server, relative_path)
        else:
            return self.file.replace(settings.SAN_PATH, settings.SAN_WEB)
            

class M3U8(models.Model):
    asset = models.ForeignKey(Asset)
    task = models.ForeignKey(Task)
    folder = models.CharField(max_length=200)
    playlist = models.CharField(max_length=100)
    created = models.DateTimeField(auto_now_add=True)
    
    def get_full_save_path(self):
        from django.conf import settings
        
        return os.path.join(settings.MEDIA_ROOT, self.get_save_path())
    
    def get_save_path(self):
        if not self.folder:
            self.folder = os.path.join('out', timezone.now().strftime('%Y/%m/%d'), str(self.asset.id), str(self.task.id), str(self.id))
            self.save()
        
        return self.folder

from django.dispatch import receiver
from django.db.models.signals import post_save
from django.contrib.humanize.templatetags.humanize import naturaltime

from kombu import Connection, Exchange
from kombu.pools import producers

from django.conf import settings

connection = Connection(settings.PROGRESS_BROKER_URL)
exchange = Exchange('amq.topic', type='topic')

@receiver(post_save, sender=Asset)
def push_asset_to_progress_queue(sender, instance, created, **kwargs):
    """
    Notify clients of the update
    """
    
    TZ = timezone.get_current_timezone()
    
    producer = producers[connection].acquire(block=True)
    publish = connection.ensure(producer, producer.publish, max_retries=3)
    
    body = {'asset': instance.pk, 'status': instance.status.title()}
    
    if created:
        logger.debug('Adding Asset "%d" to update queue.', instance.pk)
        
        body.update({
            'url': instance.get_absolute_url(),
            'slug': instance.slug,
            'is_test': instance.is_test,
            'property': instance.property.name,
            'submitted': instance.created.astimezone(TZ).strftime('%b. %d, %Y %I:%M %p')
        })
        
    else:
        logger.debug('Updating status for Asset "%d".', instance.pk)
        
        body.update({
            'progress': instance.get_progress(),
            'class': instance.progress_class(),
        })
        
    publish(body, routing_key='assets', exchange=exchange)
    producer.release()

import random

@receiver(post_save, sender=Task)
def push_task_to_progress_queue(sender, instance, created, **kwargs):
    """
    Notify clients of the update
    """
    
    TZ = timezone.get_current_timezone()
    
    producer = producers[connection].acquire(block=True)
    publish = connection.ensure(producer, producer.publish, max_retries=3)
    
    asset = instance.asset
    
    body = {'task': instance.pk, 'status': instance.status.title(), 'hostname': instance.hostname}
    
    if created:
        logger.debug('Adding Task "%d" to update queue.', instance.pk)
    else:
        logger.debug('Updating status for Task "%d".', instance.pk)
        
        body.update({
            'progress': instance.get_progress(),
            'speed': str(instance.speed) if instance.speed else None,
            'class': instance.progress_class(),
        })
        
        if 1 == random.randint(1,4):
            asset.total_progress()
    
    if instance.status == 'completed':
        outputs = []
        
        for output in Output.objects.filter(task=instance):
            outputs.append(output.local_url())
        
        body.update({'outputs': outputs})
    
    publish(body, routing_key='asset.%d' % asset.pk, exchange=exchange)
    producer.release()