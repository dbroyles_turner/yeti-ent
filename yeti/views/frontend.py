import logging

from django.shortcuts import render

from yeti.models import Asset

logger = logging.getLogger(__name__)

def main(request):
    """
    This displays the list of the assets in the system.
    """
    
    page = int(request.GET.get('page', 1))
    size = min(int(request.GET.get('size', 10)), 100) # maximum of 100
    
    total = Asset.objects.count()
    
    from math import ceil
    
    numpages = int(ceil(total / float(size)))
    
    page = max(page, 1)
    
    offset = (page - 1) * size
    end = offset + size
    
    assets = Asset.objects.select_related().order_by('-id')[offset:end]
        
    if numpages <= 10:
        pages = range(1, numpages + 1)
    else:
        if page >= numpages - 4:
            start = numpages - 9
        else:
            start = max(1, page - 5)
        
        if numpages > 10 and page <= 6:
            end = 11
        else:
            end = min(page + 5, numpages + 1)
        
        pages = range(start, end)
    
    return render(request, 'yeti/list.html', {'assets': assets, 'current': page, 'pages': pages, 'size': size})

def asset(request, id):
    """
    Display tasks associated with an Asset.    
    """
    
    asset = Asset.objects.select_related().get(id=id)
    
    tasks = []
    
    from yeti.models import Task
    
    def flatten_tasks(task):
        for t in Task.objects.filter(asset=asset, parent=task):
            tasks.append(t)
            flatten_tasks(t)
    
    flatten_tasks(None)
    
    return render(request, 'yeti/asset.html', {'asset': asset, 'tasks': tasks})

def properties(request):
    """
    Display a list of the properties in the system.
    """
    
    from yeti.models import Property
    
    properties = Property.objects.all()
    
    return render(request, 'yeti/properties.html', {'properties': properties})

def users(request):
    """
    Display a list of users in the system.
    """
    
    from django.contrib.auth.models import User
    
    users = User.objects.all()
    
    return render(request, 'yeti/users.html', {'users': users})