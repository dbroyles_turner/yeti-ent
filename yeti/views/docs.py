from django.conf import settings
from django.shortcuts import render
from django.contrib.sites.models import Site, get_current_site
from django.db.models import Q

def main(request):
    current_site = get_current_site(request)
    
    released = Q(domain__contains='yeti.turner.com')
    current = Q(pk=current_site.id)
    
    sites = Site.objects.filter(released | current).order_by('id')
    
    return render(request, 'docs/main.html', {
        'sites': sites,
        'current_site': current_site,
    })

def steps(request):
    current_site = get_current_site(request)
    
    return render(request, 'docs/steps.html', {
        'current_site': current_site,
    })

def js(request):
    current_site = get_current_site(request)
    
    return render(request, 'docs/javascript.html', {
        'current_site': current_site,
    })
