import os
import json
import logging
import os.path

from django.utils import timezone
from django.views.decorators.csrf import csrf_exempt
from django.views.decorators.http import require_POST

from yeti.models import Property, Scenario
from yeti.http import JsonResponse

logger = logging.getLogger(__name__)

def get_params(request):
    """
    Given the request, return a dictionary representing the data.
    """
    
    # We use the request content type to decide how to retrieve the JSON data.
    content_type = request.META['CONTENT_TYPE']

    logger.debug('Request content type: "%s".', content_type)
    
    # a content type of "application/json" means that the JSON is provided as the body of the request
    if content_type == 'application/json':
        params = request.body
    
    # a content type of "multipart/form-data" content type means that the JSON is provided as a POST paramater
    elif content_type.startswith('multipart/form-data'):
        if 'params' in request.POST:
            params = request.POST.get('params')
        elif 'params' in request.FILES:
            params = request.FILES['params'].read()
        else:
            for key, item in request.FILES.iteritems():
                if item.content_type == 'application/json':
                    params = item.read()
                    break
    
    elif content_type == 'application/x-www-form-urlencoded':
        return JsonResponse({'status': 'error',
            'message': 'You must include the "application/json" mimetype in the header.'}, status=500)
    
    try:
        data = json.loads(params)
    except ValueError, exc:
        logger.warning('User supplied invalid JSON data.')
        logger.warning(exc)
        logger.warning('=== Invalid JSON Payload ===\n%s', params)
        
        raise

    from django.conf import settings

    if settings.DEBUG:
        logger.debug('=== JSON Payload ===\n%s', json.dumps(data, indent=4, separators=(',', ': '), sort_keys=True))
    
    return data

from yeti.utils import asset_upload_path

def save_attachment(asset, request):
    """
    Save the first file included in the request
    """
    
    from shutil import move
    from os.path import join
    
    path = asset_upload_path(asset)
    
    for key, item in request.FILES.iteritems():
        if item.content_type != 'application/json' and key != 'params':
            logger.debug('Attachment: %s (%s) - %s', item.name, item.content_type, item.size)
            
            save_path = join(path, item.name)
            
            logger.debug('Full path to saved item "%s"', save_path)
            
            if hasattr(item, 'temporary_file_path'):
                temp_path = item.temporary_file_path()
                
                logger.debug('Renaming "%s"', temp_path)
                
                move(temp_path, save_path)
            else:
                logger.debug('Saving file...')
                
                with open(save_path, 'wb+') as dest:
                    for chunk in item.chunks():
                        dest.write(chunk)
            
            os.chmod(save_path, 0664)
                
            return key, save_path, item.name, item.content_type, item.size

def create_asset(title, slug, property_slug, scenario=None, is_test=False):
    """
    Provision an asset and return the ID.    
    """
    
    from yeti.models import Asset
    
    if property_slug:
        property = Property.objects.get(slug=property_slug)
    
        if scenario:
            scenario = Scenario.objects.get(name=scenario, property=property)
    
    import re
    
    # replace spaces with dashes
    slug = slug.lower().replace(' ', '-')
    
    # replace instances of 2 or more separating characters with 1
    slug = re.sub(r'-{2,}', '-', slug)
    slug = re.sub(r'\.{2,}', '.', slug)
    slug = re.sub(r'_{2,}', '-', slug)
    
    # remove non-URL characters [A-Za-z0-9_-.]
    slug = re.sub(r'[^\w\-\.]', '', slug)
    
    # if the slug is the same as the tile, the title is probably a slug
    # so we should try and make it look more like a title
    if slug == title:
        replacements = (
            (r'^test\.', ''),
            (r'\.cnn$', ''),
            (r'^pkg-', ''),
            (r'^cnnee-', ''),
            (r'^irpt-', ''),
            (r'^exp-', ''),
            (r'^pmt-', ''),
            (r'-', ' '),
        )
        
        for search, replace in replacements:
            title = re.sub(search, replace, title)
        
        title = title.title()
    
    asset = Asset(title=title, slug=slug, property=property, scenario=scenario, is_test=is_test, status='created')
    
    return asset

from yeti.utils import get_request_meta

@csrf_exempt
@require_POST
def asset(request):
    """
    Create the asset.
    """
    
    # default response
    response = {'status': 'accepted', 'message': 'API request has been accepted.'}
    
    # try and parse the parameters
    try:
        params = get_params(request)
    except ValueError, exc:
        return JsonResponse({'status': 'error', 'message': str(exc)}, status=500)
    
    property = params.pop('property', None)
    
    if not property:
        return JsonResponse({'status': 'error', 'message': 'You must specify a property.'}, status=500)
    
    args = (
        params.pop('title', ''), 
        params.pop('slug', ''),
        property,
        params.pop('scenario', None),
        params.pop('is_test', False),
    )
    
    url = params.pop('url', None)
    san = params.pop('san', None)
    
    try:
        asset = create_asset(*args)
    except Property.DoesNotExist:
        return JsonResponse({'status': 'error', 'message': 'The specified property does not exist.'}, status=500)
    except Scenario.DoesNotExist:
        return JsonResponse({'status': 'error', 'message': 'The specified scenario does not exist.'}, status=500)
    
    try:
        asset.save()
    except Exception:
        logger.error('Failed to create asset.', exc_info=True)
        return JsonResponse({'status': 'error', 'message': 'Failed to create asset.'}, status=500)
    
    if url:
        asset.url = url
    elif san:
        from os.path import basename, exists, getsize, join
        from django.conf import settings
        
        asset.file = join(settings.SAN_PATH, san.lstrip('/'))
        
        if not exists(asset.file):
            return JsonResponse({'status': 'error', 'message': 'The file does not exist on the SAN.'}, status=500)
        
        asset.size = getsize(asset.file)
        asset.filename = basename(asset.file)
    else:
        attachment = save_attachment(asset, request)
        
        if not attachment:
            return JsonResponse({'status': 'error', 'message': 'No file attached.'}, status=500)
        
        asset.field_name, asset.file, asset.filename, asset.content_type, asset.size = attachment
    
    source_uri = url if url else asset.file
    
    try:
        asset.save(update_fields=('url', 'file', 'size', 'filename', 'field_name', 'content_type'))
        
        from yeti.tasks.db import probe_asset, ingest, create_jobs
        
        if asset.scenario:
            steps = json.loads(asset.scenario.steps)
            params.update({'steps': steps})
        
        if url:
            workflow = ingest.s(url) | probe_asset.s() | create_jobs.s(params)
        else:
            workflow = probe_asset.s() | create_jobs.s(params)
        
        workflow.delay(asset.pk)
        
        response.update({
            'id': asset.pk,
            'status': 'created',
            'message': 'The job has been created successfully.',
            'slug': asset.slug,
            'title': asset.title,
            'property': asset.property.slug,
        })
        
    except Exception, exc:
        response['status'] = 'failed'
        
        logger.error('Failed to create asset.', exc_info=True)
        
        response.update({'status': 'error', 'message': 'Failed to create asset.'})
        
    return JsonResponse(response)

def asset_details(request, asset_id):
    """
    Asset details.
    """
    
    from yeti.models import Asset
    
    try:
        asset = Asset.objects.get(pk=asset_id)
    except Asset.DoesNotExist:
        pass
    
    return JsonResponse({
        'id': asset.pk,
        'property': asset.property.slug,
        'title': asset.title,
        'slug': asset.slug,
        'url': asset.url,
        'file_path': asset.file,
        'field_name': asset.field_name,
        'type': asset.type,
        'content_type': asset.content_type,
        'filename': asset.filename,
        'basename': asset.basename,
        'extension': asset.extension,
        'size': asset.size,
        'created': asset.created.isoformat(),
        'width': asset.width,
        'height': asset.height,
        'duration': asset.duration,
        'bit_rate': asset.bit_rate,
        'status': asset.status,
    })

def asset_tasks(request, asset_id):
    """
    Asset details.
    """
    
    from yeti.models import Asset, Task
    
    try:
        asset = Asset.objects.get(pk=asset_id)
    except Asset.DoesNotExist:
        pass
    
    tasks = []
    
    for task in Task.objects.filter(asset=asset):
        parent = task.parent.pk if task.parent else None
        
        tasks.append({
            'id': task.id,
            'status': task.status,
            'process': task.process,
            'name': task.name,
            'progress': task.progress,
            'hostname': task.hostname,
            'parent': parent,
        })
    
    return JsonResponse(tasks)

def asset_task(request, asset_id, task_id):
    """
    Asset details.
    """
    
    from yeti.models import Asset, Task
    
    try:
        task = Task.objects.get(pk=task_id, asset__id=asset_id)
    except Task.DoesNotExist:
        pass
    
    task = {
        'id': task.id,
        'status': task.status,
        'process': task.process,
        'name': task.name,
        'progress': task.progress,
        'hostname': task.hostname
    }
    
    return JsonResponse(task)

def asset_task_by_name(request, asset_id, name):
    """
    Asset details.
    """
    
    from yeti.models import Asset, Task
    
    try:
        task = Task.objects.get(name=name, asset__id=asset_id)
    except Task.DoesNotExist:
        pass
    
    task = {
        'id': task.id,
        'status': task.status,
        'process': task.process,
        'name': task.name,
        'progress': task.progress,
        'hostname': task.hostname
    }
    
    return JsonResponse(task)

def asset_outputs(request, asset_id):
    """
    Asset details.
    """
    
    from yeti.models import Asset, Output
    
    try:
        asset = Asset.objects.get(pk=asset_id)
    except Asset.DoesNotExist:
        pass
    
    outputs = []
    
    for output in Output.objects.filter(asset=asset).select_related():
        outputs.append({
            'id': output.id,
            'local_url': output.local_url(),
            'type': output.type,
            'content_type': output.content_type,
            'filename': output.filename,
            'basename': output.basename,
            'extension': output.extension,
            'size': output.size,
            'created': output.created.isoformat(),
            'width': output.width,
            'height': output.height,
            'duration': output.duration,
            'bit_rate': output.bit_rate,
            'task': output.task.name
        })
    
    return JsonResponse(outputs)

def asset_final_outputs(request, asset_id):
    """
    Asset details.
    """
    
    from yeti.models import Asset, Task, Output
    
    try:
        asset = Asset.objects.get(pk=asset_id)
    except Asset.DoesNotExist:
        pass
    
    tasks = list(Task.objects.filter(asset=asset))
    
    for task in tasks:
        if task.parent in tasks:
            tasks.remove(task.parent)
    
    outputs = []
    
    for output in Output.objects.filter(asset=asset, task__in=[task.pk for task in tasks]).select_related():
        outputs.append({
            'id': output.id,
            'local_url': output.local_url(),
            'type': output.type,
            'content_type': output.content_type,
            'filename': output.filename,
            'basename': output.basename,
            'extension': output.extension,
            'size': output.size,
            'created': output.created.isoformat(),
            'width': output.width,
            'height': output.height,
            'duration': output.duration,
            'bit_rate': output.bit_rate,
            'task': output.task.name
        })
    
    return JsonResponse(outputs)

def asset_ftp_outputs(request, asset_id):
    """
    Asset details.
    """
    
    import urlparse
    
    from yeti.models import Asset, Task, Output
    
    try:
        asset = Asset.objects.get(pk=asset_id)
    except Asset.DoesNotExist:
        pass
    
    
    outputs = []
    
    for task in Task.objects.filter(asset=asset, process='/ftp/store'):
        
        params = json.loads(task.parameters)
        
        for output in Output.objects.filter(asset=asset, task=task.parent):
            created = timezone.localtime(output.created)
            path = created.strftime(params['path'])
            full_path = os.path.join(path, output.filename)
            
            if 'resolve' in params:
                url = urlparse.urljoin(params['resolve'], full_path)
            else:
                url = None
        
            outputs.append({
                'type': output.type,
                'content_type': output.content_type,
                'filename': output.filename,
                'basename': output.basename,
                'extension': output.extension,
                'size': output.size,
                'created': output.created.isoformat(),
                'width': output.width,
                'height': output.height,
                'duration': output.duration,
                'bit_rate': output.bit_rate,
                'url': url,
            })
    
    return JsonResponse(outputs)