import logging

from celery.task.control import revoke

from django.http import HttpResponse
from django.views.decorators.csrf import csrf_exempt

from yeti.models import Task

logger = logging.getLogger(__name__)

@csrf_exempt
def stop_job(request, id):
    task = Task.objects.get(pk=id)
    
    task_id = request.POST.get('task_id')
    
    if task_id == task.amqp_id:
        revoke(task_id, terminate=True)
        
        task.status = 'stopped'
        task.save(update_fields=['status'])
    
    return HttpResponse()