import re
import json
import logging

from django.db import models
from django.utils import timezone
from django.core.exceptions import ObjectDoesNotExist

logger = logging.getLogger(__name__)

class TaskManager(models.Manager):
    def get_and_start(self, task_id, current_task):
        try:
            task = super(TaskManager, self).select_related().get(pk=task_id)
        except ObjectDoesNotExist:
            raise
        
        task.status = 'processing'
        task.progress = 1
        task.hostname = current_task.request.hostname
        task.amqp_id = current_task.request.id
        task.start_date = timezone.now()
        
        task.save(update_fields=['status', 'progress', 'hostname', 'amqp_id', 'start_date'])
        
        params = json.loads(task.parameters)
        
        return task, task.asset, params
        
STRING_SUBS = re.compile(r'(%\(.+?\)s)')

def substitute_patterns(filename, asset):
    """
    Generate a filename, subsituting any date and asset variables.    
    """
    # Added an extra percent to the beginning of the of string substitutions.
    # This is needed because datetime.strftime removes them.
    # e.g. %(id)s -> %%(id)s
    filename = STRING_SUBS.sub('%\\1', filename)
    
    # Replace date components.
    filename = asset.created.strftime(filename)
    
    # Replace asset-related variables.
    data = {'id': asset.id, 'slug': asset.slug, 'basename': asset.basename}
    filename = filename % data
    
    # Remove any path components from the filename if they exist.
    # e.g. 2012/03/ac360.obamacare.mp4 -> ac360.obamacare.mp4
    from os.path import split
    root, basename = split(filename)
    filename = basename if basename else root
    
    return filename
    
class OutputManager(models.Manager):
    def overwrite(self, task, input):
        """
        Return an output object with the same location values as the input.
        """
        kwargs = {
            'property': task.asset.property,
            'filename': input.filename,
            'asset': task.asset,
            'task': task,
            'file': input.file,
        }
        
        if hasattr(input, 'hostname'):
            kwargs['hostname'] = input.hostname
        
        return super(OutputManager, self).create(**kwargs)
        
    def create_for_filename(self, task, current_task, filename):
        """
        Given the filename, return an output.
        """
        
        filename = substitute_patterns(filename, task.asset)
        
        date = task.asset.created.strftime('outputs/%Y/%m/%d')
        structure = '%d/%d' % (task.asset.pk, task.pk)        

        from os.path import join
        
        relative_path = join(date, structure)

        from django.conf import settings
        
        absolute_path = join(settings.MEDIA_ROOT, relative_path)
        
        from os import makedirs
        
        try:
            makedirs(absolute_path)
        except OSError, exc:
            logger.error('Could not create directories.', exc_info=exc)
        
        file = join(relative_path, filename)
        
        return super(OutputManager, self).create(property=task.asset.property, asset=task.asset, task=task,
                                                 hostname=current_task.request.hostname, file=file, filename=filename)