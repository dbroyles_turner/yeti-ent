import json
import logging

from django.http import HttpResponse

logger = logging.getLogger(__name__)

class JsonResponse(HttpResponse):
    """
    JSON specific response, which allows the content paramter to be a dictionary.
    """
    
    def __init__(self, content='', **kwargs):
        """
        For the content type to be JSON.
        """
        
        kwargs['content_type'] = 'application/json'
        
        if not isinstance(content, basestring):
            content = json.dumps(content)
        
        super(JsonResponse, self).__init__(content, **kwargs)