import logging
import os.path
import mimetypes
import json

from subprocess import Popen, PIPE

from django.utils.encoding import smart_unicode
from django.db.models.fields.files import FieldFile, FileField

logger = logging.getLogger(__name__)

class AssetFieldFile(FieldFile):
    def save(self, name, content, save=True):
        super(AssetFieldFile, self).save(name, content, save=False)
        
        instance = self.instance
        
        logger.debug('Asset file being changed.')
        
        if instance.filename: # set the Asset basename
            root, ext = os.path.splitext(instance.filename)
            instance.basename = root
            
            logger.debug('Got basename "%s" from filename "%s".', root, instance.filename)
        
        logger.debug('Got "%s" as a content_type.', instance.content_type)
        
        if not instance.content_type or instance.content_type == 'application/octet-stream':
            logger.debug('Need to guess the correct MIME type.')
            
            guess = mimetypes.guess_type(instance.filename)[0]
            
            logger.debug('Best guess is "%s" based of the filename "%s".', guess, instance.filename)
            
            if guess:
                instance.content_type = guess
        
        if instance.content_type: # set the Asset type
            if instance.content_type.startswith('video'):
                instance.type = 1
            elif instance.content_type.startswith('image'):
                instance.type = 2
            elif instance.content_type.startswith('audio'):
                instance.type = 3
            elif instance.content_type == 'application/pdf':
                instance.type = 4
        
        logger.debug('Using "%s" as Asset type.', instance.get_type_display())
        
        # Save the object because it has changed, unless save is False
        if save:
            self.instance.save()
        
        if instance.type in (1,2,3) and save:
            logger.debug('Probing asset using "FFprobe".')
            
            probe = Popen(['ffprobe', '-i', instance.file.path, '-print_format', 'json', '-show_error',
                           '-show_format', '-show_streams'], stdout=PIPE, stderr=PIPE)
            stdout, stderr = probe.communicate()
            
            logger.debug(stdout)
            
            stdout = smart_unicode(stdout, errors='replace')
            data = json.loads(stdout)
            
            from yeti.models import Format, Stream, Tag
            
            # Delete previous data if it exists.
            Format.objects.filter(asset=instance).delete()
            Stream.objects.filter(asset=instance).delete()
            
            print instance.id
            
            format = Format(asset=instance)
            
            tags = data['format'].pop('tags', {})
        
            for key, value in data['format'].iteritems():
                setattr(format, key, value)
        
            format.save()
            
            for key, value in tags.iteritems():
                Tag.objects.create(content_object=format, key=key, value=value)
            
            for s in data['streams']:
                tags = s.pop('tags', {})
                s.update(asset=instance)
                
                stream = Stream(**s)
                stream.save()
                
                for key, value in tags.iteritems():
                    Tag.objects.create(content_object=stream, key=key, value=value)

class AssetFileField(FileField):
    attr_class = AssetFieldFile