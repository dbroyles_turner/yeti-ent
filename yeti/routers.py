import random

READ_DB = ('default', 'secondary')

class DBRouter(object):
    def db_for_read(self, model, **hints):
        """
        Reads go to a randomly chosen machine.
        """
        return random.choice(READ_DB)

    def db_for_write(self, model, **hints):
        """
        Writes always go to master.
        """
        return 'default'
    
    def allow_relation(self, obj1, obj2, **hints):
        """
        Allow a relation between object 1 and 2.
        """
        return True
    
    def allow_syncdb(self, db, models):
        """
        Sync all models.
        """
        return True