!function ($) {

  $(function(){

    // popovers
    $("a[data-toggle=popover]")
      .popover({
				html: true,
				placement: "right"
			})
			
      .click(function(e) {
        e.stopPropagation()
				
				// hide other popovers
				$("a[data-toggle=popover]").not(this).popover('hide')
				
				// don't hide if we click inside the popover
				$(".popover").click(function (e) {
					e.stopPropagation()
				})
				
				// but do hide if we click outside
				var $this = $(this);
				
				$(document).one('click', function (e) {
					$this.popover('hide')
				})
					
      })
		
		// tooltips
		$(".tooltips").tooltip({
	      selector: "a[data-toggle=tooltip]"
	    })

  })
  
}(window.jQuery)