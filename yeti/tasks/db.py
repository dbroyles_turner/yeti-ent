import os
import json
import os.path
import logging

from celery.task import task
from celery import current_task
from celery.utils.log import get_task_logger

from yeti.models import Asset, Task

logger = get_task_logger(__name__)

def guess_type(url):
    import mimetypes
    
    mimetypes.add_type('video/x-matroska', '.mkv', strict=True)
    mimetypes.add_type('video/x-m4v', '.m4v', strict=True)
    #mimetypes.add_type('video/mp4', '.mp4', strict=True)
    
    content_type, encoding = mimetypes.guess_type(url)
    logger.debug('Guessing for: %s', url)
    logger.debug('Guessed content type: %s', content_type)
    
    return content_type

def identify_from_url(url):
    """
    Passing a URL, identify the media.
    """
    
    logger.debug('Identifying based off the URL "%s"', url)
    
    from urlparse import urlparse
    
    o = urlparse(url)
    
    logger.debug('Making a HEAD request to determine the size and type.')
    
    import httplib
    
    conn = httplib.HTTPConnection(o.netloc)
    conn.request('HEAD', o.path, o.params)
    res = conn.getresponse()
    
    status = res.status
    
    size = res.getheader('content-length')
    content_type = res.getheader('content-type').split(';')[0]
    content_disposition = res.getheader('content-disposition')
    
    base_path = o.path
    
    from os.path import basename
    
    filename = basename(o.path)
    
    logger.debug('Path portion of URL "%s"', o.path)
    
    if content_disposition:
        import re

        match = re.search(r'filename=([^;]*)', content_disposition)

        if match:
            filename = match.group(1)
            
            logger.debug('Using filename "%s" given in "Content-Disposition" header', filename)
        else:
            logger.debug('No filename match for url: "%s", filename: "%s", content_disposition: [%s]' %(url, filename, content_disposition))
    else:
        logger.debug('No content-disposition for url: "%s"' %url)
    
    return size, content_type, filename

@task
def ingest(asset_id, url):
    try:
        asset = Asset.objects.select_related().select_for_update().get(pk=asset_id)
    except Asset.DoesNotExist, exc:
        raise ingest.retry(exc=exc, countdown=5)
    
    asset.size, asset.content_type, asset.filename = identify_from_url(url)
    
    asset.save(update_fields=['size', 'content_type', 'filename'])
    
    from yeti.utils import asset_upload_path
    
    save_path = asset_upload_path(asset)
    
    from os.path import join
    
    asset.file = join(save_path, asset.filename)
    
    asset.save(update_fields=['file'])
    
    import urllib
    
    def progress(blocknum, bs, size):
        """
        Takes in current block, block size, and size of file.
        """
        
        if size == -1: # size unreported by web server
            pass
        else:
            numblocks = float(size) / bs
            adjusted = int(numblocks / 100)
            
            if not blocknum % adjusted:
                progress = int((float(blocknum * bs) / size) * 100)
                logger.debug('Progress %d%%', progress)
    
    try:
        filename, headers = urllib.urlretrieve(url, asset.file, progress)
    except urllib.ContentTooShortError, exc:
        # Download interrupted
        # data available in exc.content
        raise
    
    return asset.pk

def probe(instance, file_location):
    from os.path import basename, splitext
    
    instance.basename, instance.extension = splitext(instance.filename)
    
    logger.debug('sibley in probe: instance: basename=%s, extension=%s' %(instance.basename, instance.extension))
    
    if instance.filename:
        if not instance.content_type or instance.content_type in ('application/octet-stream', 'text/plain'):
            instance.content_type = guess_type(instance.filename.replace('#', '1'))
    
    import subprocess
    
    probe = subprocess.Popen([
        'ffprobe', '-i', file_location.replace('#', '1'), '-print_format', 'json',
        '-show_error', '-show_format', '-show_streams'], # , '-count_frames'
        stdout=subprocess.PIPE, stderr=subprocess.PIPE)
    stdout, stderr = probe.communicate()
    
    instance.probe_output = stdout
    
    logger.debug(stdout)
    
    parsed_data = json.loads(stdout)
    
    format = parsed_data['format']
    
    instance.container_format = format['format_name']
    
    if format['format_name'] == 'image2':
        stream = parsed_data['streams'][0]
        
        instance.width = stream.get('width')
        instance.height = stream.get('height')
        instance.video_codec = stream.get('codec_name')
    else:
        instance.size = format.get('size')
        instance.bit_rate = format.get('bit_rate')
        
        duration = format.get('duration')
        
        for stream in parsed_data['streams']:
            if stream['codec_type'] == 'video':
                instance.width = stream.get('width')
                instance.height = stream.get('height')
                instance.video_codec = stream.get('codec_name')
                instance.video_frame_rate = stream.get('r_frame_rate')
                instance.video_bit_rate = stream.get('bit_rate')
                instance.sar = stream.get('sample_aspect_ratio')
                instance.dar = stream.get('display_aspect_ratio')
                
                stream_duration = stream.get('duration')
                
                # It is possible that the file duration is incorrect and most likely
                # much larger than any sane value. Therefore, if the value is larger than
                # a 4 byte unsigned integer divided by 1000 (1 ms), use the video duration
                if float(duration) > 2147483:
                    duration = stream_duration                    
            
            elif stream['codec_type'] == 'audio':
                instance.audio_codec = stream.get('codec_name')
                instance.audio_sample_rate = stream.get('sample_rate')
                instance.audio_channels = stream.get('channels')
                instance.audio_bit_rate = stream.get('bit_rate')
        
        instance.duration = float(duration) * 1000
        
        # Sometime the bit rate is not available directly
        if instance.bit_rate:
            if instance.audio_bit_rate and not instance.video_bit_rate:
                instance.video_bit_rate = int(instance.bit_rate) - int(instance.audio_bit_rate)
            elif instance.video_bit_rate and not instance.audio_bit_rate:
                instance.audio_bit_rate = int(instance.bit_rate) - int(instance.video_bit_rate)
    
    try:
        instance.save()
    except Exception, exc:
        logger.error('Problem saving metadata about output.', exc_info=True)

@task
def probe_asset(asset_id):
    """
    Using FFprobe, get the information about the asset.
    """
    try:
        asset = Asset.objects.select_for_update().get(pk=asset_id)
    except Asset.DoesNotExist, exc:
        raise probe_asset.retry(exc=exc, countdown=5)
    
    file_location = asset.get_working_path()
    
    try:
        probe(asset, file_location)
    except Exception, exc:
        asset.status = 'failed'
        asset.save(update_fields=['status'])
    
    return asset.pk

def probe_output(output):
    """
    Using FFprobe, get the information about the output.
    """
    
    file_location = output.get_working_path()
    probe(output, file_location)

@task
def ingest(url, asset_id):
    basename = os.path.basename(url)

    logger.debug('sibley in ingest - url: %s ' %(url))
    logger.debug('sibley in ingest - asset id: %s, basename: %s ' %(asset_id, basename))

    from yeti.utils import create_path, get_media_path, get_local_url
    
    base_path = create_path(asset_id, base='src')
    save_path = get_media_path(base_path)
    
    try:
        os.makedirs(save_path)
    except OSError, exc:
        pass
    
    full_save_path = os.path.join(save_path, basename)
    
    import urllib
    
    def progress(blocknum, bs, size):
        """
        Takes in current block, block size, and size of file.
        """
        
        if size == -1: # size unreported by web server
            pass
        else:
            numblocks = float(size) / bs
            adjusted = int(numblocks / 100)
            
            if not blocknum % adjusted:
                progress = int((float(blocknum * bs) / size) * 100)
                logger.debug('Progress %d%%', progress)
    
    try:
        filename, headers = urllib.urlretrieve(url, full_save_path, progress)
    except urllib.ContentTooShortError, exc:
        # Download interrupted
        # data available in exc.content
        raise
    
    host = current_task.request.hostname
    
    local_url = get_local_url(host, base_path, filename=basename)
    
    Asset.objects.filter(pk=asset_id).update(local_url=local_url, url=url, basename=basename)
    
    return local_url

def create_task_list(asset, data):
    """
    Create the task list and usage hierarchy.
    """
    
    tasklist, hierarchy = {}, {}
    
    for key, step in data['steps'].items():
        process = step.pop('process')
        use = step.pop('use', '@original')
        
        hierarchy[key] = use
        
        params = json.dumps(step)
        
        task = Task(asset=asset, name=key, process=process, parameters=params)
        
        tasklist[key] = task
        
    return tasklist, hierarchy

def save_tasks(tasklist, hierarchy):
    """
    Create the tasks in the database.
    """
    
    root = []
    
    def save_children(key, parent=None):
        """
        Recursive function to save the children of the current tasks,
        after of course saving the current beforehand.
        """
        
        for step, use in hierarchy.items():
            
            if isinstance(use, basestring):
                use = [use]
            
            for item in use:
                if item == key:
                    current = tasklist[step]
                    current.pk = None
                    current.parent = parent
                    current.save()
                    
                    if not current.parent:
                        root.append(current)
                    
                    save_children(step, current)
    
    save_children('@original')
    
    return root

def start_processing(asset, tasks):
    """
    Create the jobs for the asset.
    """
    
    for task in tasks:
        if task.process == '/video/encode':
            from yeti.tasks.video import encode as first_task
        elif task.process == '/video/hint':
            from yeti.tasks.video import hint as first_task
        elif task.process == '/video/faststart':
            from yeti.tasks.video import faststart as first_task
        elif task.process == '/video/subtitle':
            from yeti.tasks.video import subtitle as first_task
        elif task.process == '/video/thumbs':
            from yeti.tasks.video import keyframes as first_task
        else:
            # This cannot come first
            task.fail()
            continue
        
        first_task.delay(task.pk, asset)

def encode_next(parent, outputs, request=None, duration=None):
    """
    Get the next task to encode.
    """
    
    from yeti.models import Output
    
    if isinstance(outputs, Output):
        probe_output(outputs)
    else:
        for output in outputs:
            probe_output(output)
    
    tasks = Task.objects.only('process').filter(parent=parent)
    
    # there are no further jobs to process
    if not tasks.count:
        logger.debug('No more subtasks after me <Task: %d>', parent.pk)
        return
    
    for task in tasks:
        if task.process == '/video/encode':
            from yeti.tasks.video import encode as next_task
        elif task.process == '/video/hint':
            from yeti.tasks.video import hint as next_task
        elif task.process == '/video/faststart':
            from yeti.tasks.video import faststart as next_task
        elif task.process == '/video/subtitle':
            from yeti.tasks.video import subtitle as next_task
        elif task.process == '/video/thumbs':
            from yeti.tasks.video import keyframes as next_task
        elif task.process == '/ftp/store':
            from yeti.tasks.ftp import upload as next_task
        else:
             # Not a handled process.
            task.fail()
            continue
        
        next_task.apply_async(args=[task.pk, outputs], exchange='C.dq', routing_key=request.hostname)

@task(ignore_result=True)
def create_jobs(asset_id, data):
    """
    Create the jobs for the asset.
    """
    
    logger.debug('Creating jobs for Asset "%s"', asset_id)
    
    try:
        asset = Asset.objects.select_for_update().get(pk=asset_id)
    except Asset.DoesNotExist, exc:
        raise create_jobs.retry(exc=exc, countdown=5)
    
    tasklist, hierarchy = create_task_list(asset, data)
    root = save_tasks(tasklist, hierarchy)
    
    asset.status = 'waiting'
    asset.save(update_fields=['status'])
    
    start_processing(asset, root)
