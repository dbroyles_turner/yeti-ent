import os.path
import logging

from celery.task import task
from celery import current_task
from celery.utils.log import get_task_logger

from django.conf import settings

from yeti.tasks import YetiTask
from yeti.models import Task, Output
from yeti.tasks.ffmpeg import FFmpeg

logger = get_task_logger(__name__)

PRESETS = {
    'iphone': {
        'vcodec': 'libx264',
        'b:v': '600k',
        'acodec': 'libfaac',
        'b:a': '64k',
        'ac': '1',
        'profile:v': 'main',
        'level': '3',
        'f': 'mp4',
        'width': '640',
        'preset': 'medium',
        'movflags': 'faststart',
    }
}

def scaling(width, height, mod=2):
    """
    Add the scaling parameter and make sure it's divisible by two.
    """
    if width:
        width += width % mod
    
    if height:
        height += height % mod
    
    if width or height:
        if width and height:
            return 'scale=%s:%s' % (width, height)
        elif width:
            return 'scale=%s:-1' % width
        else:
            return 'scale=-1:%s' % height

@task(base=YetiTask)
def encode(task_id, input):
    """
    Encode a video.
    """
    request = current_task.request
    
    try:
        task, asset, params = Task.objects.get_and_start(task_id, current_task)
    except Task.DoesNotExist, exc:
        encode.retry(exc=exc, countdown=5)
    
    args = ['ffmpeg', '-i', input.get_working_path()]
    
    filename = params.get('filename', '%%(basename)s.mp4')
    
    # preset = params.get('preset', {})
    # 
    # if preset and preset in PRESETS:
    #     preset = PRESETS[preset]
    # elif not isinstance(preset, dict):
    #     preset = {}
    # 
    # width = preset.get('width')
    # height = preset.get('height')
    
    format = params.get('format')
    duration = params.get('duration')
    offset = params.get('offset')
    passes = params.get('passes')
    
    if format:
        args += ['-f', format]
    if duration:
        args += ['-t', str(duration)]
    if offset:
        args += ['-ss', str(offset)]
    
    audio = params.get('audio', {})
    
    bitrate = audio.get('bitrate')
    channels = audio.get('channels')
    codec = audio.get('codec')
    rate = audio.get('rate')
    
    if bitrate:
        args += ['-b:a', '%sk' % str(bitrate).rstrip('k')]
    if channels:
        args += ['-ac', str(channels)]
    if codec:
        if codec == 'aac':
            codec = 'libfaac'
        elif codec == 'amr':
            codec = 'amr_nb'
        elif codec == 'vorbis':
            codec = 'libvorbis'
        args += ['-c:a', codec]
    if rate:
        args += ['-ar', str(rate)]
    
    video = params.get('video', {})
    
    bitrate = video.get('bitrate')
    codec = video.get('codec')
    crf = video.get('crf')
    deinterlace = video.get('deinterlace')
    width = video.get('width')
    height = video.get('height')
    rate = video.get('rate')
    faststart = video.get('faststart')
    interlaced = video.get('interlaced')
    
    if bitrate:
        args += ['-b:v', '%sk' % str(bitrate).rstrip('k')]
    if codec:
        if codec == 'h264':
            codec = 'libx264'
        elif codec == 'vpx':
            codec = 'libvpx'
        args += ['-c:v', codec]
    if crf:
        args += ['-crf', str(crf)]
    if rate:
        args += ['-r', str(rate)]
    if faststart:
        args += ['-movflags', 'faststart']
    
    if interlaced:
        args += ['-flags', '+ilme+ildct', '-weightp', '0']
        scale = scaling(width, height, mod=4)
    else:
        scale = scaling(width, height)
    
    args += ['-pix_fmt', 'yuv420p']
    
    vf_args = []
    
    if deinterlace:
        vf_args.append('yadif=0:-1:0')
    
    if scale:
        vf_args.append(scale)
    
    if vf_args:
        args += ['-vf', ','.join(vf_args)]
    
    output = Output.objects.create_for_filename(task, current_task, filename)
    filename = output.get_working_path()
    
    args += [filename]
    
    ffmpeg = FFmpeg(current_task, task)
    ffmpeg.encode(args)
    
    output.ssim = ffmpeg.ssim
    output.psnr = ffmpeg.psnr
    output.rate_factor = ffmpeg.rate_factor
    output.duration = ffmpeg.input_duration
    
    output.save(update_fields=['ssim', 'psnr', 'rate_factor', 'duration'])
    
    task.complete(time=ffmpeg.encoding_time, utime=ffmpeg.cpu_time)
    
    logger.info('Finished with task "%s"', task.pk)
    
    from yeti.tasks.db import encode_next
    
    encode_next(task, output, request=request)
    
    asset.check_completion()

@task(base=YetiTask)
def keyframes(task_id, input):
    """
    Generate key frames
    """

    request = current_task.request

    try:
        task, asset, params = Task.objects.get_and_start(task_id, current_task)
        logger.info('Processing task: asset id =%s, task id=%s, process=%s' %(asset.pk, task.pk, task.process))
    except Task.DoesNotExist, exc:
        keyframes.retry(exc=exc, countdown=5)

    #json sample
    #"thumbs": {
    #"process": "/video/thumbs",
    #"width": "320",
    #"height": "180",
    #"count": "10",
    #"use": "@original",
    #"filename": "%(id)s-%(slug)s-%d-thumb.jpg"
    #},

    #from Carousel -i {{input_path}} -vsync 0 -f image2 -vf "fps=fps={{max(0.05,float(output_file_count)/input_duration)}}" -frames:v {{output_file_count}} -y {{output_path}}

    count = int(params.get('count'))

    #logger.info('keyframes: type(count)=%s, count=%s' %(type(count), count))
    #logger.info('keyframes: type(input.duration)=%s, input.duration=%s' %(type(input.duration), input.duration))

    #fps = max(0.05, (int(count) * 1.0)/input.duration)
    #logger.info('keyframes: type(fps)=%s, fps=%s' %(type(fps), fps))
    #fps = '%.3f' %fps
    #logger.info('keyframes: type(fps)=%s, fps=%s' %(type(fps), fps))

    #args = ['ffmpeg', '-i', input.get_working_path(), '-vsync', '0', '-f', 'image2', '-vf', 'fps=fps=%s' %fps, '-frames:v', count, '-y'] #is -y necessary

    filename = params.get('filename', '%%(basename)s-thumb-#.jpg')   #"filename": "%(id)s-%(slug)s-%d-thumb.jpg"

    output = Output.objects.create_for_filename(task, current_task, filename) #this is a pattern
    filename = output.get_working_path()
    filename = filename.replace('#', '%d')

    #args += [filename]

    ffmpeg = FFmpeg(current_task, task)
    #ffmpeg.encode(args)
    ffmpeg.input_duration = input.duration #if original will be None
    ffmpeg.keyframes(input.get_working_path(), count, filename)

    output.ssim = ffmpeg.ssim
    output.psnr = ffmpeg.psnr
    output.rate_factor = ffmpeg.rate_factor
    output.duration = ffmpeg.input_duration

    output.save(update_fields=['ssim', 'psnr', 'rate_factor', 'duration'])

    task.complete(time=ffmpeg.encoding_time, utime=ffmpeg.cpu_time)

    #logger.info('Finished with task "%s"', task.pk)
    logger.info('Finished with task: asset id =%s, task id=%s, process=%s' %(asset.pk, task.pk, task.process))

    from yeti.tasks.db import encode_next

    encode_next(task, output, request=request)

    asset.check_completion()


import re

MP4BOX_RE = re.compile(r'ISO File Writing:.*?\((\d+)/100\)')

@task(base=YetiTask)
def hint(task_id, input):
    """
    Hint an MP4 using MP4Box
    """
    
    try:
        task, asset, params = Task.objects.get_and_start(task_id, current_task)
    except Task.DoesNotExist, exc:
        hint.retry(exc=exc, countdown=5)
    
    overwrite = params.get('overwrite')
    filename = params.get('filename', input.filename)
    
    if overwrite:
        output = Output.objects.overwrite(task, input)
    else:
        output = Output.objects.create_for_filename(task, current_task, filename)
    
    working_path = output.get_working_path()
    
    from os.path import dirname
    
    cwd = dirname(working_path)
    
    args = ['MP4Box', '-unhint', '-hint']
    
    if not overwrite:
        args += ['-out', output.get_working_path()]
    
    args.append(input.get_working_path())
    
    logger.debug('Command line:\n%s', ' '.join(args))
    
    import subprocess
    
    process = subprocess.Popen(args, stdout=subprocess.PIPE, stderr=subprocess.PIPE, universal_newlines=True, cwd=cwd)
    
    while process.poll() is None:
        line = process.stdout.readline()
        
        match = MP4BOX_RE.search(line)
        
        if match:
            progress = int(match.group(1))
                
            print 'Progress: %d%%' % progress
            
            task.progress = progress
            task.save(update_fields=['progress'])
        else:
            print line
    
    logger.debug('Encodimg completed. Return code: "%s"', process.returncode)
    
    task.complete()
    
    from yeti.tasks.db import encode_next
    
    encode_next(task, [output], request=current_task.request)
    
    if process.returncode != 0:
        print "ERROR - ERROR"

@task(base=YetiTask)
def faststart(task_id, input):
    """
    Put the moov at the beginning of the file
    """
    
    try:
        task, asset, params = Task.objects.get_and_start(task_id, current_task)
    except Task.DoesNotExist, exc:
        faststart.retry(exc=exc, countdown=5)
    
    overwrite = params.get('overwrite')
    duration = params.get('duration', 500)
    filename = params.get('filename', input.filename)
    
    if overwrite:
        output = Output.objects.overwrite(task, input)
    else:
        output = Output.objects.create_for_filename(task, current_task, filename)
    
    working_path = output.get_working_path()
    
    from os.path import dirname
    
    cwd = dirname(working_path)
    
    args = ['MP4Box', '-inter', str(duration)]
    
    if not overwrite:
        args += ['-out', output.get_working_path()]
    
    args.append(input.get_working_path())
    
    logger.debug('Command line:\n%s', ' '.join(args))
    
    import subprocess
    
    process = subprocess.Popen(args, stdout=subprocess.PIPE, stderr=subprocess.PIPE, universal_newlines=True, cwd=cwd)
    
    while process.poll() is None:
        line = process.stdout.readline()
        
        match = MP4BOX_RE.search(line)
        
        if match:
            progress = int(match.group(1))
                
            print 'Progress: %d%%' % progress
            
            task.progress = progress
            task.save(update_fields=['progress'])
        else:
            print line
    
    logger.debug('Encodimg completed. Return code: "%s"', process.returncode)
    
    task.complete()
    
    from yeti.tasks.db import encode_next
    
    encode_next(task, [output], request=current_task.request)
    
    if process.returncode != 0:
        print "ERROR - ERROR"

@task(base=YetiTask)
def subtitle(task_id, input):
    """
    Subtitle the movie.
    """
    
    try:
        task, asset, params = Task.objects.get_and_start(task_id, current_task)
    except Task.DoesNotExist, exc:
        subtitle.retry(exc=exc, countdown=5)
    
    filename = params.get('filename', input.filename)
    subrip = params.get('subrip', {})
    
    language = subrip.get('language', 'english')
    san = subrip.get('san')
    
    output = Output.objects.create_for_filename(task, current_task, filename)
    
    working_path = output.get_working_path()
    
    from os.path import dirname, join
    
    srt_input = join(settings.SAN_PATH, san.lstrip('/'))
    
    cwd = dirname(working_path)
    
    args = ['MP4Box', '-add', '%s:hdlr=sbtl:lang=%s' % (srt_input, language),
            '-out', output.get_working_path(), input.get_working_path()]
    
    logger.debug('Command line:\n%s', ' '.join(args))
    
    import subprocess
    
    process = subprocess.Popen(args, stdout=subprocess.PIPE, stderr=subprocess.PIPE, universal_newlines=True, cwd=cwd)
    
    while process.poll() is None:
        line = process.stdout.readline()
        
        match = MP4BOX_RE.search(line)
        
        if match:
            progress = int(match.group(1))
                
            print 'Progress: %d%%' % progress
            
            task.progress = progress
            task.save(update_fields=['progress'])
        else:
            print line
    
    logger.debug('Encodimg completed. Return code: "%s"', process.returncode)
    
    task.complete()
    
    from yeti.tasks.db import encode_next
    
    encode_next(task, [output], request=current_task.request)
    
    if process.returncode != 0:
        print "ERROR - ERROR"
