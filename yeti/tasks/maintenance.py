import os.path
import subprocess

from django.conf import settings

from celery.task import task
from celery.utils.log import get_task_logger

logger = get_task_logger(__name__)

@task(ignore_result=True)
def cleanup(days=7):
    logger.debug('Performing cleanup!')
    
    logger.debug('Finding files older than %s days.' % days)
    
    args = ['find', settings.MEDIA_ROOT, '-type', 'f', '-mtime', '+%s' % days]
    
    find = subprocess.Popen(args)
    find.communicate()
    
    args.append('-delete')
    
    delete = subprocess.Popen(args)
    delete.communicate()
    
    logger.debug('Finding empty directories.')
    
    args = ['find', settings.MEDIA_ROOT, '-type', 'd', '-empty']
    
    find = subprocess.Popen(args)
    find.communicate()
    
    args.append('-delete')
    
    delete = subprocess.Popen(args)
    delete.communicate()

@task
def clean_san(days=7):
    logger.debug('Performing cleanup on the SAN!')
    
    logger.debug('Finding files older than %s days.' % days)
    
    for partition in ('news', 'sports'):
        YETI_SAN = os.path.join(settings.SAN_PATH, partition, 'yeti', 'source')
        
        args = ['find', YETI_SAN, '-type', 'f', '-mtime', '+%s' % days]
    
        find = subprocess.Popen(args)
        find.communicate()
    
        args.append('-delete')
    
        logger.debug('Finding empty directories.')
    
        args = ['find', YETI_SAN, '-type', 'd', '-empty']
    
        find = subprocess.Popen(args)
        find.communicate()
    
        args.append('-delete')
