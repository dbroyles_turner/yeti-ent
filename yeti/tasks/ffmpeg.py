import re
import time
import logging

from celery.utils.log import get_task_logger

from yeti.models import Task

logger = get_task_logger(__name__)

DURATION_RE = re.compile(r'Duration: ([\d:.]+)')
TIME_SEARCH = re.compile(r'time=([\d:.]+)')
RATE_FACTOR = re.compile(r'\] final ratefactor: ([\d.]+)')
SSIM_SEARCH = re.compile(r'\] SSIM Mean Y:([\d.]+)')
PSNR_SEARCH = re.compile(r'\] PSNR Mean Y:[\d.]+ U:[\d.]+ V:[\d.]+ Avg:[\d.]+ Global:([\d.]+)')
ENC_TIME_RE = re.compile(r'([\d.]+) real')
BMRK_SEARCH = re.compile(r'bench: utime=([\d.]+)s')

class FFmpeg(object):
    """
    A class to handle the complexities of interacting with the FFmpeg binary.
    """
    
    def __init__(self, task, task_obj):
        self.task_id = task.request.id
        self.output_duration = 0
        
        logger.debug('Task ID is "%s"', self.task_id)
        
        self.asset_id = task_obj.asset.pk
        self.task_pk = task_obj.pk
        
        self.task = task_obj
        
        # Process progress
        self.input_duration = None

        # Progress feedback
        from kombu import Connection, Exchange
        from kombu.pools import producers
        
        self.key = 'asset.%d' % task_obj.asset.pk
        
        from django.conf import settings
        
        connection = Connection(settings.PROGRESS_BROKER_URL)
        
        self.producer = producers[connection].acquire(block=True)
        self.exchange = Exchange('amq.topic', type='topic')
        
        self.progress = 0
        
        self.ssim = self.psnr = self.rate_factor = None
        self.encoding_time = self.speed = None
        
        self.cpu_time = None
        
    def __del__(self):
        self.producer.release()
    
    def parse_seconds(self, timecode):
        """
        Given a timecode, such as 00:00:31.51, return the total seconds as a float
        """
        
        h,m,s = map(float, timecode.split(':'))
        
        return (h * 3600) + (m * 60) + s
    
    def prepare(self, args):
        if '-t' in args:
            index = args.index('-t') + 1
            self.input_duration = int(args[index])
            
            logger.debug('Input duration has been overriden to %s seconds' % args[index])
        
    def encode(self, args):
        args.insert(0, 'time')
        
        self.prepare(args)
        
        output_filename = args.pop()
        
        args += ['-ssim', '1', '-psnr', '-benchmark', output_filename]
        
        logger.debug('ffmpeg encode cmd: %s' %(' '.join(args)))
        
        import subprocess
        
        self.start_time = time.time()
        
        process = subprocess.Popen(args, stdout=subprocess.PIPE, stderr=subprocess.PIPE, universal_newlines=True)
        
        while process.poll() is None:
            line = process.stderr.readline()
            
            if self.input_duration:
                self.parse_progress(line)
            else:
                self.parse_input_duration(line)
        
        self.parse_benchmark(process.stdout)
        
        logger.debug('Encodimg completed. Return code: "%s"', process.returncode)
        
        if process.returncode != 0:
            logger.error(' '.join(args))
            raise Exception('Non zero return code.')
        
        factor = '%.2f' % self.speed if self.speed else None
        logger.debug('Completed encoding factor: %s', factor)
        
        self.publish(progress=100, speed=factor, status='Complete')

    def keyframes(self, input_path, count, output_filename):
        #args.insert(0, 'time')

        #self.prepare(args)

        #output_filename = args.pop()

        #args += ['-ssim', '1', '-psnr', '-benchmark', output_filename]

        import subprocess

        if self.input_duration is None: #need to set in video.py if not None

            info_args = ['ffmpeg', '-i', input_path]

            logger.debug('ffmpeg info cmd: %s' %(' '.join(info_args)))

            #import subprocess

            #self.start_time = time.time()

            info_process = subprocess.Popen(info_args, stdout=subprocess.PIPE, stderr=subprocess.PIPE, universal_newlines=True)

            while info_process.poll() is None:
                line = info_process.stderr.readline()

                self.parse_input_duration(line)

        logger.debug('keyframe input duration: %s, %s' %(type(self.input_duration), self.input_duration))
        logger.debug('keyframe count: %s, %s' %(type(count), count))

        #import subprocess

        #from Carousel -i {{input_path}} -vsync 0 -f image2 -vf "fps=fps={{max(0.05,float(output_file_count)/input_duration)}}" -frames:v {{output_file_count}} -y {{output_path}}
        fps = max(0.05, float(count)/self.input_duration)
        fps = '%.3f' %fps
        args = ['time', 'ffmpeg', '-i', input_path, '-vsync', '0', '-f', 'image2', '-vf', 'fps=fps=%s' %fps, '-frames:v', str(count), output_filename]

        logger.debug('ffmpeg keyframes cmd: %s' %(' '.join(args)))

        self.start_time = time.time()

        process = subprocess.Popen(args, stdout=subprocess.PIPE, stderr=subprocess.PIPE, universal_newlines=True)
        #keyframe_output_lines = []
        while process.poll() is None:
            line = process.stderr.readline()
            #keyframe_output_lines.append(line.strip())

            if self.input_duration: #redundant
                self.parse_progress(line)
            else:
                self.parse_input_duration(line)

        self.parse_benchmark(process.stdout)

        logger.debug('Encodimg completed. Return code: "%s"', process.returncode)

        if process.returncode != 0:
            logger.error(' '.join(args))
            #logger.error('Output lines:')
            #logger.error('\n'.join(keyframe_output_lines))
            raise Exception('Non zero return code.')

        factor = '%.2f' % self.speed if self.speed else None
        logger.debug('Completed encoding factor: %s', factor)

        self.publish(progress=100, speed=factor, status='Complete')

    def parse_input_duration(self, line):
        """
        Parse the input duration of the video to determine progress.
        """
        
        match = DURATION_RE.search(line)
        
        if match:
            timecode = match.group(1)
            self.input_duration = self.parse_seconds(timecode)
            
            logger.debug('Got an input duration of "%s"', self.input_duration)
    
    def publish(self, **kwargs):
        """
        Publish the progress to the queue so the browser or other client can receive progress.
        """
        
        body = {'task': self.task_pk}
        
        body.update(kwargs)
        
        self.producer.publish(body, routing_key=self.key, exchange=self.exchange)
    
    def update_db(self, progress, speed):
        task = self.task
        
        if task.progress != progress:
            task.progress = progress
            task.speed = speed
            task.save(update_fields=['progress', 'speed'])
    
    def parse_benchmark(self, stdout):
        """
        After encoding, the benchmark results are put on their own line in STDOUT.
        This is used for accounting since it's the CPU time--not real time.
        """
        line = stdout.read()
        
        match = BMRK_SEARCH.search(line)
        
        if match:
            value = float(match.group(1))
            self.cpu_time = value * 1000
            
    def parse_progress(self, line):
        """
        Parse the progress.        
        """
        
        match = TIME_SEARCH.search(line)
        
        if match:
            timecode = match.group(1)
            seconds = self.parse_seconds(timecode)
            
            progress = (seconds / self.input_duration) * 100
            complete = min(int(progress), 100)
            
            logger.debug('%d%%', complete)
            
            if self.progress != complete:
                
                factor = '%.2f' % (seconds / (time.time() - self.start_time))
                logger.debug('Current encoding factor: %s', factor)
                
                self.update_db(complete, factor)
                self.progress = complete
        else:
            for exp in (RATE_FACTOR, SSIM_SEARCH, PSNR_SEARCH, ENC_TIME_RE):
                
                match = exp.search(line)
                
                if match:
                    value = match.group(1)
                    
                    if exp == RATE_FACTOR:
                        self.rate_factor = value
                    elif exp == SSIM_SEARCH:
                        self.ssim = value
                    elif exp == PSNR_SEARCH:
                        self.psnr = value
                    elif exp == ENC_TIME_RE:
                        self.encoding_time = float(value) * 1000
                        self.speed = (self.input_duration * 1000) / self.encoding_time
                    break
            else:
                print line.strip()