import os
import logging

from celery.task import task
from celery import current_task
from celery.utils.log import get_task_logger

from yeti.tasks import YetiTask
from yeti.models import Task, Output

logger = get_task_logger(__name__)

# def progress(blocknum, bs, size):
#     """
#     Takes in current block, block size, and size of file.
#     """
#     
#     if size == -1: # size unreported by web server
#         pass
#     else:
#         numblocks = float(size) / bs
#         adjusted = int(numblocks / 100)
#         
#         if not blocknum % adjusted:
#             progress = int((float(blocknum * bs) / size) * 100)
#             logger.debug('Progress %d%%', progress)

class FTPProgress(object):
    def __init__(self, total_size, task, block_size=8192):
        self.current = 0
        
        self.bs = block_size
        self.size = float(total_size)
        self.numblocks = self.size / block_size
        self.adjusted = max(int(self.numblocks / 100), 1)
        
    
    def callback(self, *args):
        self.current += 1
        
        if not self.current % self.adjusted:
            progress = int((self.current / self.numblocks) * 100)
            logger.debug('Progress %d%%', progress)
        # self.update_progress(progress)
    
    # def update_progress(self, progress):
    #     progress = int(progress)
    #     task = self.task
    #     
    #     if task.progress != progress:
    #         task.progress = progress
    #         task.save()
    #         
    #         logger.debug('Progress %s%%' % progress)

@task(base=YetiTask)
def upload(task_id, outputs):
    """
    Upload a item.
    """
    
    logger.debug('Uploading outputs via FTP. Task ID: "%s"', task_id)
    
    try:
        task, asset, params = Task.objects.get_and_start(task_id, current_task)
    except Task.DoesNotExist, exc:
        upload.retry(countdown=3)
    
    request = current_task.request
    
    Task.objects.filter(pk=task.pk).update(status='processing', hostname=request.hostname, amqp_id=request.id)
    
    import ftplib
    import socket
    
    asset = task.asset
    
    hostname = params.get('hostname')
    username = params.get('username')
    password = params.get('password')
    port = int(params.get('port', 21))
    path = params.get('path')
    
    # logger.debug(hostname)
    
    ftp = ftplib.FTP()
    
    try:
        ftp.connect(hostname, port, timeout=5)
    except socket.error, exc:
        logger.error(exc, exc_info=True)
        
        message = exc.strerror
        
        if exc.errno == 8: # 'nodename nor servname provided, or not known'
            message = 'Unknown host'
        elif exc.errno == 65: # No route to host
            message = 'Unable to reach host'
        else:
            print exc.errno
            print exc.strerror
        
        task.fail(message)
        
        raise
    
    try:
        ftp.login(username, password)
    except ftplib.error_perm, exc:
        logger.error(exc, exc_info=True)
        
        code, message = unicode(exc).split(' ', 1)
        
        task.fail(message)
        
        raise
    
    if path.startswith('/'):
        ftp.cwd('/')
    
    upload_path = path.strip('/')
    
    for char in ('Y', 'm', 'd'):
        upload_path = upload_path.replace('%%%s' % char, '%%%%%s' % char)
    
    upload_path = upload_path % {
        'id': asset.id,
        'slug': asset.slug
    }
    
    for char in ('Y', 'm', 'd'):
        upload_path = upload_path.replace('%%%%%s' % char, '%%%s' % char)
    
    from django.utils import timezone
    
    def change_directory(upload_path):
        logger.debug('Upload path: %s' % upload_path)
        
        for directory in upload_path.split('/'):
            try:
                ftp.cwd(directory)
            except ftplib.error_perm, exc:
                code, message = unicode(exc).split(' ', 1)
            
                if code == u'550': # 550 Failed to change directory.
                    ftp.mkd(directory)
                    ftp.cwd(directory)
                else:
                    logger.error(exc, exc_info=True)
                    task.fail(message)
                    raise
    
    last_path = None
    
    for output in Output.objects.filter(task=task.parent):
        date = timezone.localtime(output.created)
        path = date.strftime(upload_path)
        
        if last_path != path:
            if last_path is not None:
                ftp.cwd('/')
            change_directory(path)
        
        file_path = output.get_working_path()
        logger.debug('sibley ftp: working path: "%s"' %file_path)
        if file_path.find('#') == -1:
            file_paths = [file_path]
            total_size = os.path.getsize(file_path)
        else:
            total_size = 0
            file_paths = []
            out_base = os.path.split(file_path)[0]
            out_list = os.listdir(out_base)
            logger.debug('sibley ftp: out base: %s' %out_base)
            logger.debug('sibley ftp: out list: %s' %out_list)
            for out_file in out_list:
                out_path = os.path.join(out_base, out_file)
                if not(os.path.isdir(out_path)):
                    file_paths.append(out_path)
                    total_size += os.path.getsize(out_path)
        logger.debug('sibley ftp: file_paths: %s' %file_paths)
        logger.debug('sibley ftp: total_size: %s' %total_size)
        #total_size = os.path.getsize(file_path)
        
        progress = FTPProgress(total_size, task)

        for file_path in file_paths:
            ftp_base, ftp_filenane = os.path.split(file_path)
            with open(file_path, 'rb') as fp:
                try:
                    #ftp.storbinary('STOR %s' % output.filename, fp, 8192, progress.callback)
                    ftp.storbinary('STOR %s' % ftp_filenane, fp, 8192, progress.callback)
                except ftplib.error_perm, exc:
                    logger.error(exc, exc_info=True)

                    code, message = unicode(exc).split(' ', 1)

                    task.fail(message)

                    raise upload.retry(exc=exc, countdown=5)
    
    ftp.quit()
    
    task.complete()
    
    asset.check_completion()
    
    return None