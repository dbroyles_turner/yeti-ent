import os
import logging

from celery.task import task
from celery import current_task
from celery.utils.log import get_task_logger

from yeti.tasks import YetiTask
from yeti.models import Task, Output

logger = get_task_logger(__name__)

@task(base=YetiTask)
def san(task_id, outputs):
    pass