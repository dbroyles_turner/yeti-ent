import logging

from celery import Task

logger = logging.getLogger(__name__)

class YetiTask(Task):
    abstract = True
    
    def on_failure(self, exc, task_id, args, kwargs, einfo, status='failed'):
        """
        Add any additional error information to the task.
        """
        from yeti.models import Task
        
        try:
            task = Task.objects.select_related().get(amqp_id=task_id)
        except Task.DoesNotExist:
            logger.debug('Task ID: %s', task_id)
            logger.debug('Args: %s', args)
            return
        
        message = str(exc)
        
        if task.status != status:
            task.fail(message, status=status)
    
    def on_retry(self, *args):
        
        self.on_failure(*args, status='retrying')

    def after_return(self, status, retval, task_id, args, kwargs, einfo):
        """
        Ensure the status of a task is changed
        """
        
        logger.debug('Task "%s" finished with a status of "%s"', task_id, status)
        
        from yeti.models import Task
        
        try:
            task = Task.objects.select_related().get(amqp_id=task_id)
        except Task.DoesNotExist:
            logger.debug('Task ID: %s', task_id)
            logger.debug('Args: %s', args)
            return
        
        task.asset.total_progress()
