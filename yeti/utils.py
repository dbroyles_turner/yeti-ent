import os
import json
import os.path
import logging
import tempfile

from subprocess import Popen, PIPE

from django.core.files.base import ContentFile

from django.conf import settings
from django.utils import timezone

logger = logging.getLogger(__name__)

def asset_upload_path(asset):
    """
    Upload path for Asset source
    """
    
    from os import makedirs
    from os.path import join
    
    asset_id = str(asset.pk)
    property = asset.property
    date_part = asset.created.strftime('%Y/%m/%d')
    
    save_path = join(property.storage, 'source', property.slug, date_part, asset_id)
    
    logger.debug('Save path is "%s"', save_path)
    
    try:
        makedirs(save_path)
    except OSError, exc:
        if exc.errno == 17:
            pass # path already exists
        else:
            raise # some other error
    
    return save_path

def create_path(*args, **kwargs):
    parts = []
    
    if 'base' in kwargs:
        parts.append(kwargs['base'])
    
    now = timezone.now()
    date_structure = now.strftime('%Y/%m/%d')
    parts.append(date_structure)
    
    args = map(str, args)
    
    parts += args
    
    return '/'.join(parts)

def get_media_path(path, filename=None):
    media_path = os.path.join(settings.MEDIA_ROOT, path)
    
    try:
        os.makedirs(media_path)
    except OSError, exc:
        pass
    
    if filename:
        media_path = os.path.join(media_path, filename)
    
    return media_path

def get_local_url(host, path, filename=None):
    relative_path = os.path.join(settings.MEDIA_URL, path)
    
    if filename:
        relative_path = os.path.join(relative_path, filename)
    
    try:
        local_server = settings.LOCAL_WEB_SERVER % host
    except TypeError:
        local_server = settings.LOCAL_WEB_SERVER
    
    return '%s%s' % (local_server, relative_path)


def get_request_meta(request):    
    meta = {}
    
    properties = ('REMOTE_ADDR', 'REMOTE_HOST', 'CONTENT_LENGTH', 'CONTENT_TYPE', 'HTTP_ACCEPT', 'SERVER_NAME', 'HTTP_HOST', 'HTTP_USER_AGENT', 'PATH_INFO', 'SERVER_PORT', 'SERVER_PROTOCOL')
        
    for key in properties:
        meta[key] = request.META.get(key)
        
    data = json.dumps(meta, indent=4, sort_keys=True)
    
    return data

def preview_from_video(obj):
    output = tempfile.SpooledTemporaryFile()
    ffmpeg = Popen(['ffmpeg', '-ss', '00:00:03', '-i', obj.file.path, '-c:v', 'png', '-vframes', '1', '-pix_fmt', 'rgb24', '-f', 'image2', '-'], stdout=PIPE)
    convert = Popen(['convert', 'png:-', '-thumbnail', '@230400>', '-interlace', 'Plane', 'jpg:-'], stdin=ffmpeg.stdout, stdout=output)
    convert.communicate()
    output.seek(0)
    content = ContentFile(output.read())
    root, ext = os.path.splitext(obj.file.path)
    obj.preview.save('%s.jpg' % root, content, save=False)

def preview_from_image(obj):
    output = tempfile.SpooledTemporaryFile()
    convert = Popen(['convert', obj.file.path, '-thumbnail', '@230400>', '-interlace', 'Plane', 'jpg:-'], stdout=output)
    convert.communicate()
    output.seek(0)
    content = ContentFile(output.read())
    root, ext = os.path.splitext(obj.file.path)
    obj.preview.save('%s.jpg' % root, content, save=False)

def preview_from_pdf(obj):
    output = tempfile.SpooledTemporaryFile()
    gs = Popen(['gs', '-q', '-dSAFER', '-dBATCH', '-dNOPAUSE', '-sDEVICE=png16m', '-dGraphicsAlphaBits=4', '-dTextAlphaBits=4', '-r216', '-dDownScaleFactor=3', '-dLastPage=1', '-sOutputFile=%stdout', obj.file.path], stdout=PIPE)
    convert = Popen(['convert', 'png:-', '-thumbnail', '@230400>', '-interlace', 'Plane', 'jpg:-'], stdin=gs.stdout, stdout=output)
    convert.communicate()
    output.seek(0)
    content = ContentFile(output.read())
    root, ext = os.path.splitext(obj.file.path)
    obj.preview.save('%s.jpg' % root, content, save=False)

