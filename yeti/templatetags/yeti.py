from django import template
from django.utils import formats
from django.utils.translation import ugettext, ungettext

register = template.Library()

@register.filter(is_safe=True)
def bitrateformat(bits):
    """
    """
    try:
        bits = float(bits)
    except (TypeError,ValueError,UnicodeDecodeError):
        return ''
    
    br_num_format = lambda value: formats.number_format(round(value, 1), 1)

    KB = 10**3
    MB = 10**6
    GB = 10**9
    TB = 10**12
    PB = 10**15

    if bits < KB:
        return "%s b/s" % br_num_format(bits)
    if bits < MB:
        return "%s Kb/s" % br_num_format(bits / KB)
    if bits < GB:
        return "%s Mb/s" % br_num_format(bits / MB)
    if bits < TB:
        return "%s Gb/s" % br_num_format(bits / GB)

@register.filter(is_safe=True)
def durationformat(miliseconds):
    """
    """
    try:
        seconds = float(miliseconds) / 1000
    except (TypeError,ValueError,UnicodeDecodeError):
        return ''
    
    hours = seconds // (60*60)
    seconds %= (60*60)
    minutes = seconds // 60
    seconds %= 60
    
    return "%02i:%02i:%02i" % (hours, minutes, seconds)

@register.filter(is_safe=True)
def framerateformat(frames):
    """
    """
    
    if frames == '24000/1001':
        return '23.976 fps'
    elif frames == '30000/1001':
        return '29.97 fps'
    elif frames == '30/1':
        return '30 fps'
    else:
        return frames